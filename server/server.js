var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var maxPlayers = 4;
var playersConnected = 0;

var numLapsTotal = 3;

var names = ["PurpleBlue", "CookieNaybor", "HeartZero", "BullNothing", "RacingCheese", "TruckMcTruckface", "Drifted", "Europap", "PrizoneR"];

// id, name, isReady, isInRace, position
var playersList = [];
// id, name
var endList = [];

// Used IDs
var id = [false, false, false, false];

// 0: Waiting Room, 1: Launching Race, 2: In Race, 3: Finishing Race 
var serverState = 0;

var launchDate = 0;
var endDate = 0;

app.use(express.static(__dirname));


http.listen(8888, () => {
    console.log('Server up and running on port 8888.');
});


io.on('connection', (socket) => {

    if (playersConnected == maxPlayers) {
        socket.disconnect();
    }
    else {

        onPlayerConnects(socket);

        socket.on("disconnect", () => {
            onPlayerDisconnects(socket);
        });

        socket.on("ready", () => {
            onPlayerReady(socket);
        });

        socket.on("update", (playerUpdateInfo) => {
            onPlayerUpdates(socket, playerUpdateInfo);
        });

        socket.on("lap", () => {
            onPlayerEndsLap(socket);
        });

        socket.on("getendlist", () => {
            socket.emit("endlist", endList);
        });
    }
});


function getSlotID() {
    for (var i = 0; i < id.length; i++) {
        if (!id[i]) {
            id[i] = true;
            return i;
        }
    }

    id.push(true);
    return id.length - 1;
}


function getProceduralName() {

    var name = "";

    var isNameUsed = true;

    while (isNameUsed) {
        name = names[Math.floor(Math.random() * names.length)];

        isNameUsed = false;

        for (var i = 0; i < playersList.length; i++) {
            if (playersList[i].name === name) {
                isNameUsed = true;
                break;
            }
        }
    }

    return name;
}


function checkPlayerState() {
    var allPlayerAreReady = true;

    for (var i = 0; i < playersList.length; i++) {
        if (!playersList[i].isReady) {
            allPlayerAreReady = false;
            break;
        }
    }

    if (playersConnected > 0 && allPlayerAreReady && serverState == 0) {
        console.log("[INFO] All players are ready, launching race.");

        launchDate = (Date.now() / 1000) + 10;
        endDate = 0;
        endList = [];
        serverState = 1;

        io.emit("launchrace", launchDate);
        
        for (var i = 0; i < playersList.length; i++) {
            playersList[i].isReady = false;
            playersList[i].isInRace = true;
        }
    }
}


function updateServerState() {
    // If there is no player connected, revert to the waiting state
    if (playersConnected == 0) {
        serverState = 0;
        launchDate = 0;
        endDate = 0;
        return;
    }

    if (serverState == 1) {
        if (Date.now() / 1000 >= launchDate) {
            serverState = 2;
            launchDate = 0;
        }
    }
    else if (serverState == 3) {
        if (Date.now() / 1000 >= endDate) {
            serverState = 0;
            endDate = 0;

            for (var i = 0; i < playersList.length; i++) {
                playersList[i].isInRace = false;
                playersList[i].isReady = false;
                playersList[i].position = 0;
            }

            io.emit("playerlist", playersList);
        }
    }
}


function onPlayerConnects(socket) {
    playersConnected++;

    var playerid = getSlotID();
    var playername = getProceduralName();
    var player = { id: playerid, name: playername, isReady: false, isInRace: false, position: 0 };
    playersList.push(player);

    socket.id = playerid;
    console.log("[INFO] Player connected (ID: " + playerid + "). Server capacity: " + playersConnected + "/" + maxPlayers);

    updateServerState();

    socket.emit("ok", { id: playerid, state: serverState });

    io.emit("playerconnected", playerid, playersList);
}


function onPlayerDisconnects(socket) {
    playersConnected--;

    var playerid = socket.id;

    for (var i = 0; i < playersList.length; i++) {
        if (playersList[i].id == playerid) {
            playersList.splice(i, 1);
            break;
        }
    }

    id[playerid] = false;

    console.log("[INFO] Player disconnected (ID: " + playerid + "). Server capacity: " + playersConnected + "/" + maxPlayers);

    io.emit("playerdisconnected", playerid, playersList);

    updateServerState();

    checkPlayerState();
}


function onPlayerReady(socket) {
    var playerid = socket.id;

    // If race is launching
    if (serverState == 1) {
        for (var i = 0; i < playersList.length; i++) {
            if (playersList[i].id == playerid) {
                playersList[i].isReady = false;
                playersList[i].isInRace = true;
                break;
            }
        }

        socket.emit("launchrace", launchDate);
    }
    else if (serverState == 2) { // If race is in progress
        for (var i = 0; i < playersList.length; i++) {
            if (playersList[i].id == playerid) {
                playersList[i].isReady = false;
                playersList[i].isInRace = true;
                break;
            }
        }
        socket.emit("launchrace", (Date.now() / 1000) + 1);
    }
    else if(serverState == 0) { // If the server is waiting for players
        for (var i = 0; i < playersList.length; i++) {
            if (playersList[i].id == playerid) {
                playersList[i].isReady = true;
                break;
            }
        }

        io.emit("playerready", playerid, playersList);

        checkPlayerState();
    }
}


function onPlayerUpdates(socket, updateInfo) {
    var playerid = socket.id;
    var isInRace = false;

    for (var i = 0; i < playersList.length; i++) {
        if (playersList[i].id == playerid) {
            isInRace = playersList[i].isInRace;
            break;
        }
    }

    if (isInRace) {
        updateInfo.id = socket.id;
        io.emit("playerupdate", updateInfo);
    }

    updateServerState();
}


function onPlayerEndsLap(socket) {
    var playerid = socket.id;
    var playername = "";

    var numLaps = 0;
    
    for (var i = 0; i < playersList.length; i++) {
        if (playersList[i].id == playerid) {
            if (playersList[i].isInRace) {
                playername = playersList[i].name;
                numLaps = ++playersList[i].position;
            }
            break;
        }
    }

    if (numLaps == numLapsTotal) {
        endList.push({ id: playerid, name: playername });

        if (serverState == 2) {
            endDate = (Date.now() / 1000) + 30;
            serverState = 3;
            io.emit("endcountdown", endDate);
        }
        else if (serverState == 3) {
            var numPlayersInRace = 0;

            for (var i = 0; i < playersList.length; i++) {
                if (playersList[i].isInRace)
                    numPlayersInRace++;
            }

            if (numPlayersInRace == endList.length) {
                endDate = (Date.now() / 1000) + 3;
                io.emit("endcountdown", endDate);
            }
        }
    }
}