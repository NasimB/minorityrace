var Hud = (function () {
    function Hud(scene) {
        // A changer de place
        this.defaultLapMax = 3;
        this.bestTime = " ";
        this.rpmMinAngle = 2.3562;
        this.rpmMaxAngle = 1.98968;
        this.kphMinAngle = 2.16421;
        this.kphMaxAngle = -1.32645;
        this.boostMinAngle = 1.98968;
        this.boostMaxAngle = -1.98968;
        // A enlever et utiliser la variable renvoyer par le node
        this.nbPlayer = 1;
        this.RPMTexture = new BABYLON.Texture("assets/hud/RPM.png", scene);
        this.RPMTexture.hasAlpha = true;
        this.RPMNeedleTexture = new BABYLON.Texture("assets/hud/RPM_needle.png", scene);
        this.RPMNeedleTexture.hasAlpha = true;
        this.KPHTexture = new BABYLON.Texture("assets/hud/KPH.png", scene);
        this.KPHTexture.hasAlpha = true;
        this.KPHNeedleTexture = new BABYLON.Texture("assets/hud/KPH_needle.png", scene);
        this.KPHNeedleTexture.hasAlpha = true;
        this.BOOSTTexture = new BABYLON.Texture("assets/hud/BOOST.png", scene);
        this.BOOSTTexture.hasAlpha = true;
        this.BOOSTNeedleTexture = new BABYLON.Texture("assets/hud/BOOST_needle.png", scene);
        this.BOOSTNeedleTexture.hasAlpha = true;
    }
    Hud.prototype.lerp = function (a, b, t) {
        return a + (b - a) * t;
    };
    Hud.prototype.initHud = function (scene) {
        this.canvas = new BABYLON.ScreenSpaceCanvas2D(scene, {
            id: "HudCanvas",
            size: new BABYLON.Size(window.innerWidth, window.innerHeight)
        });
        this.initCadrans(scene);
        this.timer = new BABYLON.Text2D("00:00.00", {
            id: "text", parent: this.canvas, x: 0, y: -20,
            marginAlignment: "v: top, h: center",
            fontName: "28pt Arial Black",
        });
        this.lap = new BABYLON.Text2D("1 / 1", {
            id: "text", parent: this.canvas, x: -30, y: -20,
            marginAlignment: "v: top, h: right",
            fontName: "28pt Arial Black",
        });
        this.rank = new BABYLON.Text2D("Best Time: 00:00.00", {
            id: "text", parent: this.canvas, x: 20, y: -20,
            marginAlignment: "v: top, h: left",
            fontName: "28pt Arial Black",
        });
        this.countDown = new BABYLON.Text2D("0", {
            id: "text", parent: this.canvas, x: 0, y: 50,
            marginAlignment: "v: center, h: center",
            fontName: "40pt Arial Black",
        });
        this.countDown.levelVisible = false;
    };
    Hud.prototype.initCadrans = function (scene) {
        var scaleWidth = (window.innerWidth / 1920) * 0.9;
        var scaleHeight = (window.innerHeight / 1080) * 0.9;
        var scale = Math.min(scaleWidth, scaleHeight);
        var gaugesRect = new BABYLON.Rectangle2D({
            parent: this.canvas,
            scale: scale,
            marginAlignment: "v: center, h: center",
            width: 400,
            height: 420,
            x: (window.innerWidth / 2) - (200 * scale),
            y: -(window.innerHeight / 2) + (210 * scale)
        });
        this.RPM = new BABYLON.Sprite2D(this.RPMTexture, {
            id: "text",
            parent: gaugesRect,
            x: 0,
            y: 0,
            marginAlignment: "v: top, h: right"
        });
        this.RPMNeedle = new BABYLON.Sprite2D(this.RPMNeedleTexture, {
            parent: this.RPM,
            x: 0,
            y: 0,
            marginAlignment: "v: center, h: center"
        });
        this.KPH = new BABYLON.Sprite2D(this.KPHTexture, {
            parent: gaugesRect,
            x: 0,
            y: 0,
            marginAlignment: "v: bottom, h: left"
        });
        this.KPHNeedle = new BABYLON.Sprite2D(this.KPHNeedleTexture, {
            parent: this.KPH,
            x: 0,
            y: 0,
            marginAlignment: "v: center, h: center"
        });
        this.BOOST = new BABYLON.Sprite2D(this.BOOSTTexture, {
            parent: gaugesRect,
            x: 100,
            y: -80,
            marginAlignment: "v: center, h: center"
        });
        this.BOOSTNeedle = new BABYLON.Sprite2D(this.BOOSTNeedleTexture, {
            parent: this.BOOST,
            x: 0,
            y: 0,
            marginAlignment: "v: center, h: center"
        });
        this.speedString = new BABYLON.Text2D("000", {
            parent: this.KPH,
            x: 0,
            y: 32,
            marginAlignment: "v: bottom, h: center",
            fontName: "22pt Arial"
        });
        this.gear = new BABYLON.Text2D("0", {
            parent: this.RPM,
            x: 50,
            y: -50,
            marginAlignment: "v: center, h: center",
            fontName: "25pt Arial"
        });
    };
    Hud.prototype.setTimer = function (seconds) {
        this.timer.text = this.numberToTimer(seconds);
    };
    Hud.prototype.numberToTimer = function (seconds) {
        var min = Math.floor(seconds / 60);
        var sec = Math.floor((seconds % 60) * 100) / 100;
        return (min < 10 ? "0" : "") + min + (sec < 10 ? ":0" : ":") + sec;
    };
    Hud.prototype.timerToNumber = function (timer) {
        var result = +timer.substr(0, 2) * 60 + +timer.substr(3, 2) + +timer.substr(5, 2);
        return result;
    };
    Hud.prototype.setCountDown = function (count) {
        this.countDown.text = Math.ceil(count).toString();
        this.countDown.levelVisible = (count > 0);
    };
    Hud.prototype.setLap = function (lap, maximalLap) {
        this.lap.text = (lap + 1) + " / " + maximalLap;
    };
    Hud.prototype.setRank = function (rankValue) {
        if (rankValue === void 0) { rankValue = 1; }
        this.rank.text = "0" + rankValue + "/0" + this.nbPlayer;
    };
    Hud.prototype.setBestTime = function (bestTime) {
        this.rank.text = "Best Time: " + bestTime;
    };
    Hud.prototype.setBOOST = function (value) {
        this.BOOSTNeedle.rotation = this.lerp(this.boostMinAngle, this.boostMaxAngle, value);
    };
    Hud.prototype.setRPM = function (currentRPM, maxRPM) {
        this.RPMNeedle.rotation = this.lerp(this.rpmMinAngle, this.rpmMaxAngle, (currentRPM / 1000));
    };
    Hud.prototype.setKPH = function (currentKPH, maxKPH) {
        this.KPHNeedle.rotation = this.lerp(this.kphMinAngle, this.kphMaxAngle, (currentKPH / maxKPH));
        var speedFloored = Math.floor(Math.abs(currentKPH));
        this.speedString.text = "";
        if (speedFloored < 100)
            this.speedString.text += "0";
        if (speedFloored < 10)
            this.speedString.text += "0";
        this.speedString.text += speedFloored;
    };
    Hud.prototype.setGear = function (gear) {
        if (gear == 0)
            this.gear.text = "R";
        else if (gear == 1)
            this.gear.text = "N";
        else
            this.gear.text = (gear - 1).toString();
    };
    Hud.prototype.dispose = function () {
        this.canvas.dispose();
    };
    return Hud;
})();
//# sourceMappingURL=Hud.js.map