var MainMenu = (function () {
    function MainMenu(scene, canvas) {
        var _this = this;
        this.isVisible = false;
        this.soloButton = new BABYLON.Rectangle2D({
            parent: canvas, id: "button", x: 0, y: 75, width: 300, height: 40,
            fill: "#000000FF", roundRadius: 10,
            marginAlignment: "h: center, v: center",
            children: [
                new BABYLON.Text2D("Singleplayer", { marginAlignment: "h: center, v: center" }),
            ]
        });
        this.soloButton.pointerEventObservable.add(function (d, s) {
            _this.onSingleplayerClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);
        this.multiButton = new BABYLON.Rectangle2D({
            parent: canvas, id: "button2", x: 0, y: 0, width: 300, height: 40,
            fill: "#000000FF", roundRadius: 10,
            marginAlignment: "h: center, v: center",
            children: [
                new BABYLON.Text2D("Multiplayer", { marginAlignment: "h: center, v: center" })
            ]
        });
        this.multiButton.pointerEventObservable.add(function (d, s) {
            _this.onMultiplayerClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);
        this.controlsButton = new BABYLON.Rectangle2D({
            parent: canvas, id: "button2", x: 0, y: -75, width: 300, height: 40,
            fill: "#000000FF", roundRadius: 10,
            marginAlignment: "h: center, v: center",
            children: [
                new BABYLON.Text2D("Controls", { marginAlignment: "h: center, v: center" })
            ]
        });
        this.controlsButton.pointerEventObservable.add(function (d, s) {
            _this.onControlsClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);
    }
    ;
    MainMenu.prototype.dispose = function () {
        this.soloButton.dispose();
        this.multiButton.dispose();
    };
    MainMenu.prototype.hide = function () {
        this.soloButton.levelVisible = false;
        this.multiButton.levelVisible = false;
        this.controlsButton.levelVisible = false;
        this.isVisible = false;
    };
    MainMenu.prototype.show = function () {
        this.soloButton.levelVisible = true;
        this.multiButton.levelVisible = true;
        this.controlsButton.levelVisible = true;
        this.isVisible = true;
    };
    return MainMenu;
})();
//# sourceMappingURL=MainMenu.js.map