var MenuSoloEnd = (function () {
    function MenuSoloEnd(scene, canvas) {
        var _this = this;
        this.isVisible = false;
        this.titleText = new BABYLON.Text2D("Results", { parent: canvas, marginAlignment: "h: center, v: top", fontName: "40pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y: -150 });
        this.retryButton = new BABYLON.Rectangle2D({
            id: "retryButton", x: 0, y: -100, width: 300, height: 40,
            fill: "#000000FF", roundRadius: 10,
            children: [
                new BABYLON.Text2D("Play Again", { marginAlignment: "h: center, v:center" })
            ]
        });
        this.retryButton.pointerEventObservable.add(function (d, s) {
            _this.onRetryButtonClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);
        this.mainMenuButton = new BABYLON.Rectangle2D({
            id: "mainMenuButton", x: 0, y: -150, width: 300, height: 40,
            fill: "#000000FF", roundRadius: 10,
            children: [
                new BABYLON.Text2D("Back", { marginAlignment: "h: center, v:center" })
            ]
        });
        this.mainMenuButton.pointerEventObservable.add(function (d, s) {
            _this.onMainMenuButtonClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);
        this.timeList = new BABYLON.Rectangle2D({
            parent: canvas, id: "timeList", width: 300, height: 250,
            fill: "#000000FF", roundRadius: 10,
            marginAlignment: "h: center, v: center",
            children: [
                this.firstTime = new BABYLON.Text2D("First lap: 00:00:00", { marginAlignment: "h: left, v:top", y: -10, x: 10 }),
                this.secondTime = new BABYLON.Text2D("Second lap: 00:00:00", { marginAlignment: "h: left, v:top", y: -60, x: 10 }),
                this.thirdTime = new BABYLON.Text2D("Third lap: 00:00:00", { marginAlignment: "h: left, v:top", y: -110, x: 10 }),
                this.retryButton,
                this.mainMenuButton,
            ]
        });
    }
    MenuSoloEnd.prototype.hide = function () {
        this.titleText.levelVisible = false;
        this.timeList.levelVisible = false;
        this.retryButton.levelVisible = false;
        this.mainMenuButton.levelVisible = false;
        this.isVisible = false;
    };
    MenuSoloEnd.prototype.show = function () {
        this.titleText.levelVisible = true;
        this.timeList.levelVisible = true;
        this.retryButton.levelVisible = true;
        this.mainMenuButton.levelVisible = true;
        this.isVisible = true;
    };
    return MenuSoloEnd;
})();
//# sourceMappingURL=MenuSoloEnd.js.map