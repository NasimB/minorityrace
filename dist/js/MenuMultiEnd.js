var MenuMultiEnd = (function () {
    function MenuMultiEnd(scene, canvas) {
        var _this = this;
        this.isVisible = false;
        this.titleText = new BABYLON.Text2D("Results", { parent: canvas, marginAlignment: "h: center, v: top", fontName: "40pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y: -150 });
        this.roomButton = new BABYLON.Rectangle2D({
            id: "roomButton", y: -100, width: 300, height: 40,
            fill: "#000000FF", roundRadius: 10,
            children: [
                new BABYLON.Text2D("Play again", { marginAlignment: "h: center, v:center" })
            ]
        });
        this.roomButton.pointerEventObservable.add(function (d, s) {
            _this.onRetryButtonClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);
        this.mainMenuButton = new BABYLON.Rectangle2D({
            id: "mainMenuButton", y: -150, width: 300, height: 40,
            fill: "#000000FF", roundRadius: 10,
            children: [
                new BABYLON.Text2D("Back to menu", { marginAlignment: "h: center, v:center" })
            ]
        });
        this.mainMenuButton.pointerEventObservable.add(function (d, s) {
            _this.onMainMenuButtonClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);
        this.playerList = new BABYLON.Text2D("1st - AZERTYUIOP\n\n2nd - AZERTYUIOP\n\n3rd - AZERTYUIOP\n\n4th - AZERTYUIOP", { marginAlignment: "h: left, v:top", x: 10, y: -10 });
        this.rankList = new BABYLON.Rectangle2D({
            parent: canvas, id: "timeList", width: 300, height: 250,
            fill: "#000000FF", roundRadius: 10,
            marginAlignment: "h: center, v: center",
            children: [
                this.playerList,
                this.roomButton,
                this.mainMenuButton,
            ]
        });
    }
    MenuMultiEnd.prototype.hide = function () {
        this.titleText.levelVisible = false;
        this.rankList.levelVisible = false;
        this.roomButton.levelVisible = false;
        this.mainMenuButton.levelVisible = false;
        this.isVisible = false;
    };
    MenuMultiEnd.prototype.show = function () {
        this.titleText.levelVisible = false;
        this.rankList.levelVisible = true;
        this.roomButton.levelVisible = true;
        this.mainMenuButton.levelVisible = true;
        this.isVisible = true;
    };
    MenuMultiEnd.prototype.setRank = function (list) {
        this.playerList.text = "1st - " + list[0].name + " (" + list[0].id + ") " + (list[0].id == Socket.id ? "(You) \n\n" : "\n\n");
        if (list.length > 1)
            this.playerList.text += "2nd - " + list[1].name + " (" + list[1].id + ") " + (list[1].id == Socket.id ? "(You)\n\n" : "\n\n");
        if (list.length > 2)
            this.playerList.text += "3rd - " + list[2].name + " (" + list[2].id + ") " + (list[1].id == Socket.id ? "(You)\n\n" : "\n\n");
        if (list.length > 3)
            this.playerList.text += "4th - " + list[3].name + " (" + list[3].id + ") " + (list[1].id == Socket.id ? "(You)" : "");
    };
    return MenuMultiEnd;
})();
//# sourceMappingURL=MenuMultiEnd.js.map