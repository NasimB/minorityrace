/// <reference path=".\typings\babylon.d.ts" />
/// <reference path=".\Socket.ts" />
/// <reference path=".\MainMenu.ts" />
/// <reference path=".\MenuMultiStart.ts" />
/// <reference path=".\MenuSoloEnd.ts" />
/// <reference path=".\MenuMultiEnd.ts" />
/// <reference path=".\MenuControls.ts" />
var MenuManager = (function () {
    function MenuManager() {
        this.playerList = null;
    }
    MenuManager.prototype.initMenu = function (scene) {
        var _this = this;
        // Init HUD
        this.hud = new Hud(scene);
        this.hud.initHud(scene);
        // Init Menus
        this.canvas = this.createCanvas(scene);
        window.addEventListener('resize', function () {
            if (_this.hud != null) {
                _this.hud.dispose();
            }
            _this.hud = new Hud(scene);
            _this.hud.initHud(scene);
            if (_this.canvas != null) {
                _this.canvas.dispose();
            }
            _this.canvas = _this.createCanvas(scene);
            _this.menuMultiStart.setPlayerList(_this.playerList);
        });
        this.mainMenu.show();
        this.menuMultiStart.hide();
        this.menuMultiEnd.hide();
        this.menuSoloEnd.hide();
        this.menuControls.hide();
    };
    MenuManager.prototype.createCanvas = function (scene) {
        var mainMenuActive = false;
        var menuMultiStartActive = false;
        var menuSoloEndActive = false;
        var menuMultiEndActive = false;
        var menuControlsActive = false;
        var canvasActive = true;
        if (this.canvas != null) {
            canvasActive = this.canvas.levelVisible;
        }
        if (this.mainMenu != null) {
            mainMenuActive = this.mainMenu.isVisible;
        }
        if (this.menuMultiStart != null) {
            menuMultiStartActive = this.menuMultiStart.isVisible;
        }
        if (this.menuMultiEnd != null) {
            menuMultiEndActive = this.menuMultiEnd.isVisible;
        }
        if (this.menuSoloEnd != null) {
            menuSoloEndActive = this.menuSoloEnd.isVisible;
        }
        if (this.menuControls != null) {
            menuControlsActive = this.menuControls.isVisible;
        }
        var tmpCanvas = new BABYLON.ScreenSpaceCanvas2D(scene, { id: "MenuCanvas", size: new BABYLON.Size(window.innerWidth, window.innerHeight) });
        this.menuBackground = new BABYLON.Rectangle2D({
            parent: tmpCanvas,
            x: 0,
            y: 0,
            width: window.innerWidth,
            height: window.innerHeight,
            fill: "#FFFFFFFF",
            marginAlignment: "h: center, v: center"
        });
        var scaleWidth = (window.innerWidth / 1920);
        var scaleHeight = (window.innerHeight / 1080);
        var scale = Math.min(scaleWidth, scaleHeight);
        this.titleText = new BABYLON.Text2D("MINORITY RACE", {
            parent: this.menuBackground,
            marginAlignment: "h: center, v:top",
            fontName: Math.floor(scale * 60) + "pt Arial Black",
            defaultFontColor: new BABYLON.Color4(0, 0, 0, 1),
            y: -50 * scale
        });
        this.creditText = new BABYLON.Text2D("Made by:\nNasim BOUGUERRA\nPaul LOUMOUAMOU\nMusic by:\nFabio CENTRACCHIO", {
            parent: this.menuBackground,
            marginAlignment: "h: left, v:bottom",
            fontName: Math.floor(scale * 18) + "pt Arial",
            defaultFontColor: new BABYLON.Color4(0, 0, 0, 1),
            x: 5 * scale,
            y: 5 * scale
        });
        this.initMainMenu(scene, tmpCanvas);
        this.initMenuMultiStart(scene, tmpCanvas);
        this.initMenuMultiEnd(scene, tmpCanvas);
        this.initMenuSoloEnd(scene, tmpCanvas);
        this.initMenuControls(scene, tmpCanvas);
        mainMenuActive ? this.mainMenu.show() : this.mainMenu.hide();
        menuMultiStartActive ? this.menuMultiStart.show() : this.menuMultiStart.hide();
        menuMultiEndActive ? this.menuMultiEnd.show() : this.menuMultiEnd.hide();
        menuSoloEndActive ? this.menuSoloEnd.show() : this.menuSoloEnd.hide();
        menuControlsActive ? this.menuControls.show() : this.menuControls.hide();
        tmpCanvas.levelVisible = canvasActive;
        return tmpCanvas;
    };
    MenuManager.prototype.initMainMenu = function (scene, canvas) {
        var _this = this;
        this.mainMenu = new MainMenu(scene, canvas);
        this.mainMenu.onSingleplayerClicked = function () {
            _this.mainMenu.hide();
            _this.canvas.levelVisible = false;
            _this.onLaunchSingleplayerRace();
            _this.menuSoloEnd.show();
        };
        this.mainMenu.onMultiplayerClicked = function () {
            _this.mainMenu.hide();
            Socket.connect();
            Socket.on("ok", function (info) {
                _this.menuMultiStart.show();
                _this.menuMultiStart.setServerState(info.state);
            });
            Socket.on("playerconnected", function (playerid, playerlist) {
                _this.menuMultiStart.setPlayerList(playerlist);
                _this.playerList = playerlist;
            });
            Socket.on("playerdisconnected", function (playerid, playerlist) {
                _this.menuMultiStart.setPlayerList(playerlist);
                _this.playerList = playerlist;
            });
            Socket.on("playerlist", function (playerlist) {
                _this.menuMultiStart.setPlayerList(playerlist);
                _this.playerList = playerlist;
            });
            Socket.on("playerready", function (playerid, playerlist) {
                _this.menuMultiStart.setPlayerList(playerlist);
                _this.playerList = playerlist;
            });
            Socket.on("launchrace", function (date) {
                _this.menuMultiStart.hide();
                _this.canvas.levelVisible = false;
                _this.onLaunchMultiplayerRace(date);
            });
        };
        this.mainMenu.onControlsClicked = function () {
            _this.mainMenu.hide();
            _this.menuControls.show();
        };
    };
    MenuManager.prototype.initMenuMultiStart = function (scene, canvas) {
        var _this = this;
        this.menuMultiStart = new MenuMultiStart(scene, canvas);
        this.menuMultiStart.onReadyButtonClicked = this.onReadyButtonClicked;
        this.menuMultiStart.onBackButtonClicked = function () {
            _this.menuMultiStart.hide();
            _this.mainMenu.show();
            Socket.disconnect();
        };
    };
    MenuManager.prototype.onReadyButtonClicked = function () {
        Socket.emit("ready", null);
    };
    MenuManager.prototype.initMenuSoloEnd = function (scene, canvas) {
        var _this = this;
        this.menuSoloEnd = new MenuSoloEnd(scene, canvas);
        this.menuSoloEnd.onMainMenuButtonClicked = function () {
            _this.menuSoloEnd.hide();
            _this.mainMenu.show();
        };
    };
    MenuManager.prototype.initMenuMultiEnd = function (scene, canvas) {
        var _this = this;
        this.menuMultiEnd = new MenuMultiEnd(scene, canvas);
        this.menuMultiEnd.onMainMenuButtonClicked = function () {
            _this.menuMultiEnd.hide();
            _this.mainMenu.show();
            Socket.disconnect();
        };
    };
    MenuManager.prototype.initMenuControls = function (scene, canvas) {
        var _this = this;
        this.menuControls = new MenuControls(scene, canvas);
        this.menuControls.onBackButtonClicked = function () {
            _this.menuControls.hide();
            _this.mainMenu.show();
        };
    };
    MenuManager.prototype.onEndCountdownFinished = function () {
        var _this = this;
        this.canvas.levelVisible = true;
        this.menuMultiEnd.show();
        Socket.emit("getendlist", null);
        Socket.on("endlist", function (list) {
            _this.menuMultiEnd.setRank(list);
        });
        this.menuMultiEnd.onRetryButtonClicked = function () {
            _this.menuMultiEnd.hide();
            _this.menuMultiStart.show();
        };
    };
    MenuManager.prototype.onSoloRaceFinished = function (times) {
        var _this = this;
        this.canvas.levelVisible = true;
        this.menuSoloEnd.show();
        this.menuSoloEnd.firstTime.text = times[0];
        this.menuSoloEnd.secondTime.text = times[1];
        this.menuSoloEnd.thirdTime.text = times[2];
        this.menuSoloEnd.onRetryButtonClicked = function () {
            _this.menuSoloEnd.hide();
            _this.canvas.levelVisible = false;
            _this.onLaunchSingleplayerRace();
        };
    };
    return MenuManager;
})();
//# sourceMappingURL=MenuManager.js.map