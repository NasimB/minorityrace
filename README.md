#Minority Race
## A small arcade racing game using BabylonJS and Typescript

Made by: 
Nasim Bouguerra
Paul Loumouamou
Sounds by Fabio Centracchio

Base Magione track by Assetto Corsa team


## Features

- Basic car physics
- Node.JS multiplayer
- X360 Gamepad support
- UI and HUD using Canvas2D
- CannonJS physics engine for track collision detection and response
- PBR pipeline