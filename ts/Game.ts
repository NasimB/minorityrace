/// <reference path=".\typings\babylon.d.ts" />
/// <reference path=".\typings\babylon.skyMaterial.d.ts" />
/// <reference path=".\Socket.ts" />
/// <reference path=".\Car.ts" />
/// <reference path=".\Hud.ts" />
/// <reference path=".\CheckpointManager.ts" />
/// <reference path=".\GamepadController.ts" />
/// <reference path=".\KeyboardController.ts" />
/// <reference path=".\MenuManager.ts" />


class Game {

    public MAX_PLAYERS: number = 4;
    public version: number = 5;

    public engine: BABYLON.Engine;
    public scene: BABYLON.Scene;

    private hdrSeamlessTexture;

    public car: Car;
    public myID: number = 0;

    public frame: number = 0;
    public frametimes: number[] = [];

    public cars: Car[] = [];

    public carMesh: BABYLON.Mesh;
    public trackMesh: BABYLON.Mesh;

    public lightDirection: BABYLON.Vector3;

    public skybox: BABYLON.Mesh;

    public shadowCar: BABYLON.ShadowGenerator;

    public camera: BABYLON.FollowCamera;
    public oldCarForward: BABYLON.Vector3;

    private gamepad: GamepadController;
    private keyboard: KeyboardController;

    public checkpointManager: CheckpointManager;

    public physicsPlugin: BABYLON.CannonJSPlugin;

    public menuSound: BABYLON.Sound;
    public raceSound: BABYLON.Sound;

    public menu: MenuManager;

    public positions = [
        new BABYLON.Vector3(-65.3, 5.1, 96.5),
        new BABYLON.Vector3(-56.5, 5.1, 91.6),
        new BABYLON.Vector3(-57.5, 5.1, 82.5),
        new BABYLON.Vector3(-49.2, 5.1, 80.0)
    ];

    public timer: number = 0;
    private isTimerLoop: boolean = true;
    private timeToSecond: number = 0;

    private bestTime: number = 0;

    public nbPlayer: number = 1;

    private checkpoints: BABYLON.Mesh[] = [];

    private controllable: boolean = false;

    private launchTime: number = 0;
    private endTime: number = 0;


    private laps: number = 0;
    private maxLaps: number = 3;

    private firstLapTime: string;
    private secondLapTime: string;
    private thirdLapTime: string;


    private lerp(a: number, b: number, t: number) {
        var clampedT = Math.min(Math.max(t, 0), 1);
        return a + (b - a) * clampedT;
    }


    private logLerp(a: number, b: number, t: number) {
        var clampedT = Math.min(Math.max(t, 0), 1);
        var PI = 3.14159265359;
        var expT = Math.sin(clampedT * PI * 0.5);
        return a + (b - a) * expT;
    }


    private expLerp(a: number, b: number, t: number) {
        var clampedT = Math.min(Math.max(t, 0), 1);
        var PI = 3.14159265359;
        var expT = 1 - Math.cos(clampedT * PI * 0.5);
        return a + (b - a) * expT;
    }


    constructor(canvasId: string) {
        let canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById(canvasId);
        this.engine = new BABYLON.Engine(canvas, true);

        BABYLON.Engine.ShadersRepository = "shaders/";

        for (var i = 0; i < this.MAX_PLAYERS; i++) {
            this.cars.push(null);
        }

        this.scene = null;

        window.addEventListener("resize", () => {
            this.engine.resize();
        });

        this._run();
    }


    private stopTimer() {
        this.isTimerLoop = false;
    }


    private startTimer() {
        this.isTimerLoop = true;
    }


    private resetTimer() {
        this.timer = 0;
        this.timeToSecond = 0;
    }


    private _run() {
        this.engine.loadingUIText = "MINORITY RACE: Downloading assets... It might take some time.";
        this.engine.displayLoadingUI();

        this._initScene();

        this.gamepad = new GamepadController();
        this.keyboard = new KeyboardController();

        // Remove loader
        var loaderDiv = <HTMLElement>document.querySelector("#loader");
        loaderDiv.style.display = "none";

        var loader: BABYLON.AssetsManager = new BABYLON.AssetsManager(this.scene);

        // LOAD TRACK
        var meshTask = loader.addMeshTask('magione', '', 'assets/magione/', 'magione.babylon');
        meshTask.onSuccess = (task) => {

            var taskMesh: BABYLON.MeshAssetTask = task as BABYLON.MeshAssetTask;
            this.trackMesh = taskMesh.loadedMeshes[0] as BABYLON.Mesh;

            for (var i = 0; i < taskMesh.loadedMeshes.length; i++) {
                taskMesh.loadedMeshes[i].freezeWorldMatrix();

                if (taskMesh.loadedMeshes[i].name.substring(0, 3) == "col") {
                    taskMesh.loadedMeshes[i].parent = null;
                    taskMesh.loadedMeshes[i].physicsImpostor = new BABYLON.PhysicsImpostor(taskMesh.loadedMeshes[i],
                        BABYLON.PhysicsImpostor.BoxImpostor, { mass: 0 }, this.scene);
                    taskMesh.loadedMeshes[i].visibility = 0;
                }
                else if (taskMesh.loadedMeshes[i].name.substring(0, 10) == "checkpoint") {
                    this.checkpoints.push(taskMesh.loadedMeshes[i] as BABYLON.Mesh);
                }
                else {
                    var standard: BABYLON.StandardMaterial = taskMesh.loadedMeshes[i].material as BABYLON.StandardMaterial;
                    if (standard != null) {
                        var pbr: BABYLON.PBRMaterial = new BABYLON.PBRMaterial("pbr", this.scene);

                        pbr.albedoColor = standard.diffuseColor;

                        if (standard.diffuseTexture != null) {
                            pbr.albedoTexture = standard.diffuseTexture;
                            pbr.useAlphaFromAlbedoTexture = false;
                        }

                        if (standard.name == "glass") {
                            pbr.microSurface = 0.95;
                            pbr.reflectivityColor = new BABYLON.Color3(0.5, 0.5, 0.5);
                        }
                        else if (standard.name == "asphalt") {
                            pbr.microSurface = 0.3;
                            pbr.useAutoMicroSurfaceFromReflectivityMap = true;
                            pbr.reflectivityColor = new BABYLON.Color3(0.12, 0.12, 0.12);
                            pbr.bumpTexture = new BABYLON.Texture("assets/magione/asphalt_normal.jpg", this.scene);
                            pbr.ambientTexture = new BABYLON.Texture("assets/magione/asphalt_occlusion.jpg", this.scene);
                        }
                        else if (standard.name == "grass") {
                            pbr.microSurface = 0.35;
                            pbr.reflectivityColor = new BABYLON.Color3(0.12, 0.12, 0.12);
                            pbr.bumpTexture = new BABYLON.Texture("assets/magione/grass_normal.jpg", this.scene);
                            pbr.ambientTexture = new BABYLON.Texture("assets/magione/grass_occlusion.jpg", this.scene);
                        }
                        else {
                            pbr.microSurface = 0.2;
                            pbr.reflectivityColor = new BABYLON.Color3(0.1, 0.1, 0.1);
                        }

                        pbr.reflectionTexture = this.hdrSeamlessTexture;

                        taskMesh.loadedMeshes[i].material = pbr;
                    }
                }
            }
        };

        // LOAD CAR
        meshTask = loader.addMeshTask('mondeo', '', 'assets/mondeo/', 'car.babylon');
        meshTask.onSuccess = (task) => {
            var taskMesh: BABYLON.MeshAssetTask = task as BABYLON.MeshAssetTask;
            this.carMesh = taskMesh.loadedMeshes[0] as BABYLON.Mesh;
            this.carMesh.visibility = 0;
            this.carMesh.scaling = new BABYLON.Vector3(0.01, 0.01, 0.01);
            this.carMesh.position = new BABYLON.Vector3(1000, -1000, 1000);

            for (var i = 0; i < taskMesh.loadedMeshes.length; i++) {
                var standard: BABYLON.StandardMaterial = taskMesh.loadedMeshes[i].material as BABYLON.StandardMaterial;
                if (standard != null) {
                    var pbr: BABYLON.PBRMaterial = new BABYLON.PBRMaterial("pbr", this.scene);

                    pbr.albedoColor = standard.diffuseColor;

                    if (standard.diffuseTexture != null) {
                        pbr.albedoTexture = standard.diffuseTexture;
                        pbr.useAlphaFromAlbedoTexture = false;
                    }

                    if (standard.name == "glass") {
                        pbr.microSurface = 0.95;
                        pbr.reflectivityColor = new BABYLON.Color3(0.5, 0.5, 0.5);
                    }
                    else if (standard.name == "body") {
                        pbr.microSurface = 0.6;
                        pbr.useAutoMicroSurfaceFromReflectivityMap = true;
                        pbr.reflectivityColor = new BABYLON.Color3(0.3, 0.3, 0.3);
                    }
                    else {
                        pbr.microSurface = 0.2;
                        pbr.reflectivityColor = new BABYLON.Color3(0.1, 0.1, 0.1);
                    }

                    pbr.reflectionTexture = this.hdrSeamlessTexture;

                    taskMesh.loadedMeshes[i].material = pbr;
                }
            }
        };

        loader.onTaskError = (task) => {
            console.error("[ERROR] Error while downloading assets!");
        };

        this.raceSound = new BABYLON.Sound("RaceSound", "assets/sounds/music_race.wav", this.scene, null, { loop: true, autoplay: false, volume: 1 });
        this.menuSound = new BABYLON.Sound("MenuSound", "assets/sounds/music_menu.wav", this.scene, null, { loop: true, autoplay: false, volume: 1 });

        loader.onFinish = () => {
            this._initGame();

            this.scene.meshes[0].receiveShadows = false;

            for (var i = 1; i < this.scene.meshes.length; i++) {
                let mesh = this.scene.meshes[i];

                if (mesh.parent == this.car.mesh) {
                    this.shadowCar.getShadowMap().renderList.push(mesh);
                    mesh.receiveShadows = false;
                }
                else if (mesh != this.car.mesh && mesh != this.trackMesh) {
                    mesh.receiveShadows = true;
                }
            }

            this.checkpointManager = new CheckpointManager();
            this.checkpointManager.initCheckpoints(this.scene, this.checkpoints, this.car);

            this.engine.runRenderLoop(() => { this._updateGame(); this.scene.render(); });

            this.engine.hideLoadingUI();

            //this.menuSound.play();
        };

        // Démarre le chargement
        loader.load();
    }


    private _initScene() {
        this.scene = new BABYLON.Scene(this.engine);

        var gravityVector = new BABYLON.Vector3(0, -9.81, 0);
        this.physicsPlugin = new BABYLON.CannonJSPlugin();
        this.scene.enablePhysics(gravityVector, this.physicsPlugin);

        this.lightDirection = new BABYLON.Vector3(-1, -0.15, -0.048);
        this.lightDirection = this.lightDirection.normalize();

        this.scene.fogEnabled = true;
        this.scene.fogMode = BABYLON.Scene.FOGMODE_EXP2;
        this.scene.fogDensity = 0.005;
        this.scene.fogColor = new BABYLON.Color3(70 / 255, 71 / 255, 76 / 255);

        this.camera = new BABYLON.FollowCamera("FollowCamera", new BABYLON.Vector3(0, 0, 0), this.scene);
        this.camera.attachControl(this.engine.getRenderingCanvas());
        this.camera.maxZ = 600;

        var sun = new BABYLON.DirectionalLight("Dir0", this.lightDirection, this.scene);
        sun.intensity = 1;
        sun.diffuse = new BABYLON.Color3(1, 1, 196 / 255);
        sun.specular = new BABYLON.Color3(1, 1, 196 / 255);

        this.shadowCar = new BABYLON.ShadowGenerator(256, sun);
        this.shadowCar.useBlurVarianceShadowMap = true;

        var lightPos: BABYLON.Vector3 = this.lightDirection.scale(-100);

        this.skybox = BABYLON.Mesh.CreateBox("skyBox", 1.0, this.scene);
        this.hdrSeamlessTexture = new BABYLON.HDRCubeTexture("assets/skyboxes/skybox.rgbe", this.scene, 512, false, false, false, false);
        var hdrSkyboxMaterial = new BABYLON.PBRMaterial("skyBox", this.scene);
        hdrSkyboxMaterial.backFaceCulling = false;
        hdrSkyboxMaterial.fogEnabled = false;
        hdrSkyboxMaterial.reflectionTexture = this.hdrSeamlessTexture.clone();
        hdrSkyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
        hdrSkyboxMaterial.microSurface = 1.0;
        hdrSkyboxMaterial.cameraExposure = 0.66;
        hdrSkyboxMaterial.cameraContrast = 1.66;
        this.skybox.material = hdrSkyboxMaterial;

        var groundCollision: BABYLON.Mesh = BABYLON.MeshBuilder.CreateBox('cube0', { width: 1000, height: 10, depth: 1000 }, this.scene);
        groundCollision.visibility = 0;
        groundCollision.position = new BABYLON.Vector3(0, 0, 10);
        groundCollision.physicsImpostor = new BABYLON.PhysicsImpostor(groundCollision, BABYLON.PhysicsImpostor.BoxImpostor, { mass: 0, restitution: 0.9 }, this.scene);
    }


    private _initGame() {
        //this.scene.debugLayer.show();

        this.car = new Car(this.carMesh);
        this.oldCarForward = new BABYLON.Vector3(0, 0, 1);
        this.car.mesh.position = this.positions[this.myID % 4].clone();
        this.car.mesh.rotationQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(2.6, 0, 0);

        this.menu = new MenuManager();
        this.menu.initMenu(this.scene);
        this.menuSound.play();

        this.menu.onLaunchMultiplayerRace = (date) => {

            this.myID = Socket.id;

            this.launchTime = date;
            this.stopTimer();
            this.resetTimer();

            this.laps = 0;

            this.car.boostQuantity = 1;

            this.menu.hud.setLap(this.laps, this.maxLaps);
            this.menu.hud.setBestTime(this.menu.hud.numberToTimer(this.bestTime));

            this.car.mesh.physicsImpostor.setLinearVelocity(new BABYLON.Vector3(0, 0, 0));
            this.car.mesh.physicsImpostor.setAngularVelocity(new BABYLON.Vector3(0, 0, 0));
            this.car.mesh.position = this.positions[this.myID % 4].clone();
            this.car.mesh.rotationQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(2.6, 0, 0);

            Socket.on('playerupdate', (infos) => {
                var id = infos.id;

                if (id != this.myID && infos.version == this.version) {
                    if (this.cars[id] == null && this.carMesh != null) {
                        this.cars[id] = new Car(this.carMesh);
                    }

                    if (this.cars[id] != null) {
                        /*
                        version: this.version,
                        position: this.car.mesh.position,
                        rotation: this.car.mesh.rotationQuaternion,
                        velocity: this.car.velocity,
                        currentGear: this.car.currentGear,
                        now: Date.now(),
                        throttle: throttleValue,
                        brake: [0 - 1],
                        steer: [-1 - 1],
                        boost: [true - false];
                        */

                        var car: Car = this.cars[id];

                        var newPos: BABYLON.Vector3 = new BABYLON.Vector3(infos.position.x, 5.0, infos.position.z);
                        if (BABYLON.Vector3.Distance(car.mesh.position, newPos) > 0.3)
                            car.mesh.setAbsolutePosition(newPos);

                        car.mesh.rotationQuaternion = new BABYLON.Quaternion(infos.rotation.x, infos.rotation.y, infos.rotation.z, infos.rotation.w);

                        car.mesh.physicsImpostor.setLinearVelocity(new BABYLON.Vector3(infos.velocity.x, 0.0, infos.velocity.z));

                        car.currentGear = infos.currentGear;

                        car.setSteer(infos.steer);
                        car.setThrottle(infos.throttle);
                        car.setBrake(infos.brake);
                        car.setBoost(infos.boost);

                        /*var infoNow: number = infos.now;
                        var now: number = Date.now();
                        var past: number = new Date(infoNow).getTime();

                        var delta: number = Math.abs(now - past) / 1000000;
                        car.update(delta);*/
                    }
                }
            });

            Socket.on('playerconnected', (playerID) => {
                console.log("[INFO] Un joueur s'est connecte (ID: " + playerID + ")");
            });

            Socket.on('playerdisconnected', (playerID) => {
                if (playerID != this.myID && this.cars[playerID] != null) {
                    console.log("[INFO] Un joueur s'est deconnecte (ID: " + playerID + ")");

                    this.cars[playerID].dispose();
                    this.cars[playerID] = null;
                }
            });

            Socket.on('endcountdown', (endTime) => {
                this.endTime = endTime;
            });

            if (this.raceSound.isPlaying)
                this.raceSound.stop();

            if (this.menuSound.isPlaying)
                this.menuSound.stop();

            this.raceSound.play();

            this.car.enableEngineSound();
        };


        this.menu.onLaunchSingleplayerRace = () => {
            this.resetTimer();
            this.stopTimer();

            this.launchTime = (Date.now() / 1000) + 5;

            this.controllable = false;

            this.laps = 0;
            this.menu.hud.setLap(this.laps, this.maxLaps);
            this.menu.hud.setBestTime(this.menu.hud.numberToTimer(this.bestTime));
            this.car.boostQuantity = 1;

            this.car.mesh.physicsImpostor.setLinearVelocity(new BABYLON.Vector3(0, 0, 0));
            this.car.mesh.physicsImpostor.setAngularVelocity(new BABYLON.Vector3(0, 0, 0));
            this.car.mesh.position = this.positions[this.myID % 4].clone();
            this.car.mesh.rotationQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(2.6, 0, 0);

            if (this.raceSound.isPlaying)
                this.raceSound.stop();

            if (this.menuSound.isPlaying)
                this.menuSound.stop();

            this.raceSound.play();

            this.car.enableEngineSound();
        };
    }


    private finish() {

        if (this.menu.hud.timerToNumber(this.menu.hud.timer.text) < this.bestTime || this.bestTime == 0) {
            this.bestTime = this.menu.hud.timerToNumber(this.menu.hud.timer.text);
            this.menu.hud.setBestTime(this.menu.hud.timer.text);
        }

        this.resetTimer();
        this.car.resetProgression();
    }


    private _updateGame() {

        if (this.launchTime > 0) {
            var now = Date.now() / 1000;
            var delta = this.launchTime - now;

            this.menu.hud.setCountDown(delta);

            if (delta <= 0) {
                this.launchTime = 0;
                this.startTimer();
                this.controllable = true;
            }
        }
        else if (this.endTime > 0) {
            var now = Date.now() / 1000;
            var delta = this.endTime - now;

            this.menu.hud.setCountDown(delta);

            if (delta <= 0) {
                this.endTime = 0;
                this.stopTimer();
                this.controllable = false;

                this.car.disableEngineSound();

                if (this.raceSound.isPlaying)
                    this.raceSound.stop();

                if (this.menuSound.isPlaying)
                    this.menuSound.stop();

                this.menuSound.play();

                this.menu.onEndCountdownFinished();
            }
        }

        if (this.checkpointManager != null) {
            if (this.checkpointManager.verifyCheckpoints(this.car)) {
                this.finish();
                this.laps++;

                this.menu.hud.setLap(this.laps, this.maxLaps);

                if (Socket.isConnected) {
                    Socket.emit("lap", this.laps);
                }
                else {
                    switch (this.laps) {
                        case 1:
                            this.firstLapTime = this.menu.hud.timer.text;
                            break;
                        case 2:
                            this.secondLapTime = this.menu.hud.timer.text;
                            break;
                        case 3:
                            this.thirdLapTime = this.menu.hud.timer.text;
                            break;
                    }

                    if (this.laps == this.maxLaps) {
                        this.controllable = false;
                        this.launchTime = 0;
                        this.endTime = 0;
                        this.stopTimer();
                        this.menu.onSoloRaceFinished(["First lap: " + this.firstLapTime, "Second lap: " + this.secondLapTime, "Third lap: " + this.thirdLapTime]);

                        if (this.raceSound.isPlaying)
                            this.raceSound.stop();

                        if (this.menuSound.isPlaying)
                            this.menuSound.stop();

                        this.menuSound.play();

                        this.car.disableEngineSound();
                    }
                }
            };
        }

        // FRAMETIME SMOOTHER
        var deltaTime: number = Math.min(Math.max(this.engine.getDeltaTime() / 1000, 1 / 200), 1);

        var smoothDeltaTime: number = 0;
        this.frametimes.push(deltaTime);

        if (this.frametimes.length > 10)
            this.frametimes.shift();

        var weight: number = 1 / this.frametimes.length;
        for (var i = 0; i < this.frametimes.length; i++) {
            smoothDeltaTime += this.frametimes[i] * weight;
        }

        deltaTime = smoothDeltaTime;

        this.physicsPlugin.setTimeStep(deltaTime);

        if (smoothDeltaTime > 0.2 && this.camera.maxZ > 200) {
            this.camera.maxZ = Math.max(this.camera.maxZ - 10, 200);
            this.skybox.scaling = new BABYLON.Vector3(this.camera.maxZ - 1, this.camera.maxZ - 1, this.camera.maxZ - 1);
        }
        else if (smoothDeltaTime < 0.16667 && this.camera.maxZ < 2000) {
            this.camera.maxZ = Math.min(this.camera.maxZ + 10, 2000);
            this.skybox.scaling = new BABYLON.Vector3(this.camera.maxZ - 1, this.camera.maxZ - 1, this.camera.maxZ - 1);
        }


        this.gamepad.update();

        var throttleValue: number = this.gamepad.isGamepadConnected ? this.gamepad.rightTrigger : (this.keyboard.isThrottlePressed ? 1 : 0);
        var brakeValue: number = this.gamepad.isGamepadConnected ? this.gamepad.leftTrigger : (this.keyboard.isBrakePressed ? 1 : 0);
        var steerValue: number = this.gamepad.isGamepadConnected ? this.gamepad.directionX : (this.keyboard.isSteerLeftPressed ? -1 : (this.keyboard.isSteerRightPressed ? 1 : 0));
        var boostValue: boolean = this.gamepad.isGamepadConnected ? this.gamepad.boost : this.keyboard.isBoostPressed;

        if (!this.controllable) {
            throttleValue = 0;
            brakeValue = 0;
            steerValue = 0;
            boostValue = false;
        }

        this.car.setSteer(steerValue);
        this.car.setThrottle(throttleValue);
        this.car.setBrake(brakeValue);
        this.car.setBoost(boostValue);


        this.car.update(deltaTime);

        this.menu.hud.setGear(this.car.currentGear);
        this.menu.hud.setRPM(this.car.lerpedRPM, this.car.maxRPM);
        this.menu.hud.setKPH(this.car.speedKPH, this.car.maxSpeedKPH);
        this.menu.hud.setBOOST(this.car.boostQuantity);

        if (this.isTimerLoop) {
            this.timer += this.engine.getDeltaTime() / 1000;
            this.menu.hud.setTimer(this.timer);
        }
        else {
            this.menu.hud.setTimer(0);
        }


        this.shadowCar.getLight().position = this.car.mesh.position.add(this.lightDirection.scale(5));

        this.oldCarForward = BABYLON.Vector3.Lerp(this.oldCarForward, this.car.forward, 1 - Math.pow(0.1, deltaTime));

        this.camera.setTarget(this.car.mesh.position.add(this.car.forward.scale(-1).add(new BABYLON.Vector3(0, 2, 0))));

        var distance = this.expLerp(8, 5.5, this.car.speedKPH / this.car.maxSpeedKPH);

        var newPos = this.car.mesh.position.add(this.oldCarForward.scale(distance).add(new BABYLON.Vector3(0, 2.5, 0)));
        this.camera.position = newPos;
        this.camera.fov = this.expLerp(0.873, 1.55473, this.car.speedKPH / this.car.maxSpeedKPH);

        this.skybox.position = this.camera.position.subtract(new BABYLON.Vector3(0, this.camera.position.y, 0));

        for (var i = 0; i < this.MAX_PLAYERS; i++) {
            if (this.cars[i] != null) {
                var distance = BABYLON.Vector3.Distance(this.camera.position, this.cars[i].mesh.absolutePosition);
                if (this.camera.isInFrustum(this.cars[i].mesh) || distance < 25) {
                    this.cars[i].update(deltaTime);
                }

                if (this.camera.isInFrustum(this.cars[i].mesh) && distance < 40) {
                    this.cars[i].enableParticles();
                }
                else {
                    this.cars[i].disableParticles();
                }
            }
        }

        if (Socket.isConnected) {
            this.frame += deltaTime;
            if (this.frame > (1 / 4)) {
                this.frame = 0;

                Socket.emit("update", {
                    version: this.version,
                    position: this.car.mesh.getAbsolutePosition(),
                    rotation: this.car.mesh.rotationQuaternion,
                    velocity: this.car.mesh.physicsImpostor.getLinearVelocity(),
                    currentGear: this.car.currentGear,
                    now: Date.now(),
                    throttle: throttleValue,
                    brake: brakeValue,
                    steer: steerValue,
                    boost: boostValue
                });
            }
        }
    }
} 
