class Hud {

    private canvas: BABYLON.Canvas2D;

    private speedString: BABYLON.Text2D;

    private RPMTexture: BABYLON.Texture;
    private RPMNeedleTexture: BABYLON.Texture;

    private RPM: BABYLON.Sprite2D;
    private RPMNeedle: BABYLON.Sprite2D;

    private KPHTexture: BABYLON.Texture;
    private KPHNeedleTexture: BABYLON.Texture;

    private KPH: BABYLON.Sprite2D;
    private KPHNeedle: BABYLON.Sprite2D;

    private BOOSTTexture: BABYLON.Texture;
    private BOOSTNeedleTexture: BABYLON.Texture;

    private BOOST: BABYLON.Sprite2D;
    private BOOSTNeedle: BABYLON.Sprite2D;

    private gear: BABYLON.Text2D;

    private lap: BABYLON.Text2D;

    // A changer de place
    private defaultLapMax: number = 3;

    public timer: BABYLON.Text2D;

    private rank: BABYLON.Text2D;
    public bestTime: string = " ";

    private countDown: BABYLON.Text2D;

    private rpmMinAngle: number = 2.3562;
    private rpmMaxAngle: number = 1.98968;

    private kphMinAngle: number = 2.16421;
    private kphMaxAngle: number = -1.32645;

    private boostMinAngle: number = 1.98968;
    private boostMaxAngle: number = -1.98968;

    // A enlever et utiliser la variable renvoyer par le node
    private nbPlayer: number = 1;


    private lerp(a: number, b: number, t: number) {
        return a + (b - a) * t;
    }


    constructor(scene: BABYLON.Scene) {
        this.RPMTexture = new BABYLON.Texture("assets/hud/RPM.png", scene);
        this.RPMTexture.hasAlpha = true;

        this.RPMNeedleTexture = new BABYLON.Texture("assets/hud/RPM_needle.png", scene);
        this.RPMNeedleTexture.hasAlpha = true;

        this.KPHTexture = new BABYLON.Texture("assets/hud/KPH.png", scene);
        this.KPHTexture.hasAlpha = true;

        this.KPHNeedleTexture = new BABYLON.Texture("assets/hud/KPH_needle.png", scene);
        this.KPHNeedleTexture.hasAlpha = true;

        this.BOOSTTexture = new BABYLON.Texture("assets/hud/BOOST.png", scene);
        this.BOOSTTexture.hasAlpha = true;

        this.BOOSTNeedleTexture = new BABYLON.Texture("assets/hud/BOOST_needle.png", scene);
        this.BOOSTNeedleTexture.hasAlpha = true;
    }


    public initHud(scene: BABYLON.Scene) {
        this.canvas = new BABYLON.ScreenSpaceCanvas2D(scene, {
            id: "HudCanvas",
            size: new BABYLON.Size(window.innerWidth, window.innerHeight)
        });

        this.initCadrans(scene);

        this.timer = new BABYLON.Text2D("00:00.00", {
            id: "text", parent: this.canvas, x: 0, y: -20,
            marginAlignment: "v: top, h: center",
            fontName: "28pt Arial Black",
        });

        this.lap = new BABYLON.Text2D("1 / 1", {
            id: "text", parent: this.canvas, x: -30, y: -20,
            marginAlignment: "v: top, h: right",
            fontName: "28pt Arial Black",
        })

        this.rank = new BABYLON.Text2D("Best Time: 00:00.00", {
            id: "text", parent: this.canvas, x: 20, y: -20,
            marginAlignment: "v: top, h: left",
            fontName: "28pt Arial Black",
        });

        this.countDown = new BABYLON.Text2D("0", {
            id: "text", parent: this.canvas, x: 0, y: 50,
            marginAlignment: "v: center, h: center",
            fontName: "40pt Arial Black",
        });

        this.countDown.levelVisible = false;
    }


    private initCadrans(scene: BABYLON.Scene) {

        var scaleWidth = (window.innerWidth / 1920) * 0.9;
        var scaleHeight = (window.innerHeight / 1080) * 0.9;
        var scale = Math.min(scaleWidth, scaleHeight);

        var gaugesRect: BABYLON.Rectangle2D = new BABYLON.Rectangle2D({
            parent: this.canvas,
            scale: scale,
            marginAlignment: "v: center, h: center",
            width: 400,
            height: 420,
            x: (window.innerWidth / 2) - (200 * scale),
            y: -(window.innerHeight / 2) + (210 * scale)
        });

        this.RPM = new BABYLON.Sprite2D(this.RPMTexture, {
            id: "text",
            parent: gaugesRect,
            x: 0,
            y: 0,
            marginAlignment: "v: top, h: right"
        });


        this.RPMNeedle = new BABYLON.Sprite2D(this.RPMNeedleTexture, {
            parent: this.RPM,
            x: 0,
            y: 0,
            marginAlignment: "v: center, h: center"
        });


        this.KPH = new BABYLON.Sprite2D(this.KPHTexture, {
            parent: gaugesRect,
            x: 0,
            y: 0,
            marginAlignment: "v: bottom, h: left"
        });


        this.KPHNeedle = new BABYLON.Sprite2D(this.KPHNeedleTexture, {
            parent: this.KPH,
            x: 0,
            y: 0,
            marginAlignment: "v: center, h: center"
        });


        this.BOOST = new BABYLON.Sprite2D(this.BOOSTTexture, {
            parent: gaugesRect,
            x: 100,
            y: -80,
            marginAlignment: "v: center, h: center"
        });


        this.BOOSTNeedle = new BABYLON.Sprite2D(this.BOOSTNeedleTexture, {
            parent: this.BOOST,
            x: 0,
            y: 0,
            marginAlignment: "v: center, h: center"
        });


        this.speedString = new BABYLON.Text2D("000", {
            parent: this.KPH,
            x: 0,
            y: 32,
            marginAlignment: "v: bottom, h: center",
            fontName: "22pt Arial"
        });


        this.gear = new BABYLON.Text2D("0", {
            parent: this.RPM,
            x: 50,
            y: -50,
            marginAlignment: "v: center, h: center",
            fontName: "25pt Arial"
        });
    }


    public setTimer(seconds: number) {
        this.timer.text = this.numberToTimer(seconds);
    }


    public numberToTimer(seconds: number): string {
        var min: number = Math.floor(seconds / 60);
        var sec: number = Math.floor((seconds % 60) * 100) / 100;
        return (min < 10 ? "0" : "") + min + (sec < 10 ? ":0" : ":") + sec;
    }


    public timerToNumber(timer: string): number {
        var result: number = +timer.substr(0, 2) * 60 + +timer.substr(3, 2) + +timer.substr(5, 2);
        return result;
    }


    public setCountDown(count: number) {
        this.countDown.text = Math.ceil(count).toString();
        this.countDown.levelVisible = (count > 0);
    }


    public setLap(lap: number, maximalLap: number) {
        this.lap.text = (lap + 1) + " / " + maximalLap;
    }


    public setRank(rankValue: number = 1) {
        this.rank.text = "0" + rankValue + "/0" + this.nbPlayer;
    }


    public setBestTime(bestTime: string) {
        this.rank.text = "Best Time: " + bestTime;
    }


    public setBOOST(value: number) {
        this.BOOSTNeedle.rotation = this.lerp(this.boostMinAngle, this.boostMaxAngle, value);
    }


    public setRPM(currentRPM: number, maxRPM: number) {
        this.RPMNeedle.rotation = this.lerp(this.rpmMinAngle, this.rpmMaxAngle, (currentRPM / 1000));
    }


    public setKPH(currentKPH: number, maxKPH: number) {
        this.KPHNeedle.rotation = this.lerp(this.kphMinAngle, this.kphMaxAngle, (currentKPH / maxKPH));

        var speedFloored = Math.floor(Math.abs(currentKPH));
        this.speedString.text = "";
        if (speedFloored < 100)
            this.speedString.text += "0";
        if (speedFloored < 10)
            this.speedString.text += "0";

        this.speedString.text += speedFloored;
    }


    public setGear(gear: number) {
        if (gear == 0)
            this.gear.text = "R";
        else if (gear == 1)
            this.gear.text = "N";
        else
            this.gear.text = (gear - 1).toString();
    }


    public dispose(): void {
        this.canvas.dispose();
    }
}