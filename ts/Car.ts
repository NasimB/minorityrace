class Car {

    public isFinishLinePassed: boolean = false;
    public isCheckpoint2Passed: boolean = false;
    public isCheckpoint3Passed: boolean = false;

    // CAR SETTINGS
    private gearRatios = [3.437, 0.00, 3.626, 2.2, 1.541, 1.213, 1.0, 0.767]; // SR20DETT gearbox
    private cDrag: number = 0.4257; // drag coefficient
    private cRr: number = 30 * this.cDrag; // rolling resistance 
    private engineMaxTorque: number = 500; //274; // SR20DETT max torque
    private engineMinTorque: number = 350; //220;
    private brakeForce: number = 13000;
    private maxSteeringAngle: number = 0.733038; // 40�
    private engineVaccum: number = 3000; // engine brake 
    private differentialRatio: number = 5.5; // 3.692; // Nissan S15 diffratio
    private transmissionEfficiency: number = 0.7;
    private wheelRadius: number = 0.34;
    private wheelDistance: number = 4; // distance between front and back wheels 
    private mass: number = 1250; // Nissan S15 mass
    public maxSpeedKPH: number = 200.0;
    public maxRPM: number = 8000; // Nissan S15 redline
    private boostTorqueMultiplier: number = 2.0;

    private wheelsPositions = [
        new BABYLON.Vector3(-0.8, 0, -2),
        new BABYLON.Vector3(-0.8, 0, 2),
        new BABYLON.Vector3(0.8, 0, -2),
        new BABYLON.Vector3(0.8, 0, 2)
    ];


    public currentRr: number = this.cRr;
    public currentGear: number = 1; // 0: Reverse, 1: Neutral, 2: 1st
    public rpm: number = 0;
    private oldRPM: number = 0;
    public lerpedRPM: number = 0;
    public boostQuantity: number = 1;

    public steeringAngle: number = 0.0;
    public speed: number = 0.0;
    public speedKPH: number = 0.0;
    
    private steerAmount: number = 0;
    private throttleAmount: number = 0;
    private brakeAmount: number = 0;
    private isBoostPressed: boolean = false;

    private clutchSlip: number = 0.0;

    public mesh: BABYLON.Mesh;
    public forward: BABYLON.Vector3;
    public velocity: BABYLON.Vector3;
    public enginesound: BABYLON.Sound;

    private wheels: BABYLON.AbstractMesh[] = [];
    private frontWheels: BABYLON.AbstractMesh[] = [];
    private wheelsParticles: BABYLON.ParticleSystem[] = [];

    private rearLights: BABYLON.AbstractMesh;
    private rearLightsMaterial: BABYLON.PBRMaterial;

    private exhausts: BABYLON.AbstractMesh[] = [];
    private exhaustParticles: BABYLON.ParticleSystem[] = [];



    constructor(mesh: BABYLON.Mesh) {
        this.mesh = mesh.clone("mondeo", null, false, false);

        this.mesh.physicsImpostor = new BABYLON.PhysicsImpostor(this.mesh, BABYLON.PhysicsImpostor.BoxImpostor, { mass: 1100, /*restitution: 0, */friction: 0 }, this.mesh.getScene());

        var childrens: BABYLON.AbstractMesh[] = this.mesh.getChildMeshes();
        for (var i = 0; i < childrens.length; i++) {
        
            if (childrens[i].name.substring(7, 12) == "wheel") {
                this.wheels.push(childrens[i]);
                if (childrens[i].name.substring(13, 18) == "front") {
                    this.frontWheels.push(childrens[i]);
                }
            }
            else if (childrens[i].name.substring(7, 18) == "rear_lights") {
                this.rearLights = childrens[i];
                this.rearLightsMaterial = this.rearLights.material.clone("rear_lights") as BABYLON.PBRMaterial;
                this.rearLights.material = this.rearLightsMaterial;
            }
            else if (childrens[i].name.substring(7, 14) == "exhaust") {
                this.exhausts.push(childrens[i]);
            }
        }

        this.forward = new BABYLON.Vector3(0, 0, 0);
        this.velocity = new BABYLON.Vector3(0, 0, 0);

        this.enginesound = new BABYLON.Sound("Engine", "assets/sounds/engine.wav", this.mesh.getScene(), null, { volume: 0.4, loop: true, autoplay: false, spatialSound: true, distanceModel: "exponential", refDistance: 20, rolloffFactor: 2 });
        this.enginesound.attachToMesh(this.mesh);

        for (var i = 0; i < this.exhausts.length; i++) {

            var particleSystem = new BABYLON.ParticleSystem("particles", 250, this.mesh.getScene());
            particleSystem.particleTexture = new BABYLON.Texture("assets/fx/smoke.png", this.mesh.getScene());
            particleSystem.emitter = this.mesh;
            particleSystem.minEmitBox = new BABYLON.Vector3(-0.1, -0.1, -0.1); // Starting all From
            particleSystem.maxEmitBox = new BABYLON.Vector3(0.1, 0.1, 0.1); // To...
            particleSystem.minLifeTime = 0.3;
            particleSystem.maxLifeTime = 1.5;
            particleSystem.minSize = 0.1;
            particleSystem.maxSize = 0.5;
            particleSystem.emitRate = 0;
            particleSystem.minEmitPower = 1;
            particleSystem.maxEmitPower = 3;
            particleSystem.updateSpeed = 0.005;
            particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_ONEONE;
            particleSystem.gravity = new BABYLON.Vector3(0, 3, 0);
            particleSystem.direction1 = new BABYLON.Vector3(0, 0, -1);
            particleSystem.direction2 = new BABYLON.Vector3(0, 0, -1);
            particleSystem.minAngularSpeed = 0;
            particleSystem.maxAngularSpeed = Math.PI;
            particleSystem.color1 = new BABYLON.Color4(0.7, 0.8, 1.0, 1.0);
            particleSystem.color2 = new BABYLON.Color4(0.2, 0.5, 1.0, 1.0);
            particleSystem.colorDead = new BABYLON.Color4(0, 0, 0, 0.0);
            particleSystem.disposeOnStop = false;
            particleSystem.start();

            this.exhaustParticles.push(particleSystem);
        }

        for (var i = 0; i < this.wheels.length; i++) {

            var particleSystem = new BABYLON.ParticleSystem("particles", 500, this.mesh.getScene());
            particleSystem.particleTexture = new BABYLON.Texture("assets/fx/smoke.png", this.mesh.getScene());
            particleSystem.emitter = new BABYLON.Vector3(0, 6, 0);
            particleSystem.minEmitBox = new BABYLON.Vector3(-0.1, -0.1, -0.1); // Starting all From
            particleSystem.maxEmitBox = new BABYLON.Vector3(0.1, 0.1, 0.1); // To...
            particleSystem.minLifeTime = 0.3;
            particleSystem.maxLifeTime = 0.8;
            particleSystem.minSize = 0.5;
            particleSystem.maxSize = 1.5;
            particleSystem.emitRate = 0;
            particleSystem.minEmitPower = 1;
            particleSystem.maxEmitPower = 3;
            particleSystem.updateSpeed = 0.005;
            particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_ONEONE;
            particleSystem.gravity = new BABYLON.Vector3(0, 1, 0);
            particleSystem.direction1 = this.velocity.scale(-1);
            particleSystem.direction2 = this.velocity.scale(-1);
            particleSystem.minAngularSpeed = 0;
            particleSystem.maxAngularSpeed = Math.PI;
            particleSystem.color1 = new BABYLON.Color4(0.7, 0.8, 1.0, 1.0);
            particleSystem.color2 = new BABYLON.Color4(0.2, 0.5, 1.0, 1.0);
            particleSystem.colorDead = new BABYLON.Color4(0, 0, 0, 0.0);
            particleSystem.disposeOnStop = false;
            particleSystem.start();

            this.wheelsParticles.push(particleSystem);
        }
    }

    public resetProgression() {
        this.isCheckpoint2Passed = false;
        this.isCheckpoint3Passed = false;
        this.isFinishLinePassed  = false;
    }

    public dispose() {
        this.mesh.dispose();
        this.enginesound.dispose();

        for (var i = 0; i < this.exhaustParticles.length; i++) {
            this.exhaustParticles[i].dispose();
        }

        for (var i = 0; i < this.wheelsParticles.length; i++) {
            this.wheelsParticles[i].dispose();
        }
    }

    private lerp(a: number, b: number, t: number) {
        var clampedT = Math.min(Math.max(t, 0), 1);
        return a + (b - a) * clampedT;
    }

    private lerpUnclamped(a: number, b: number, t: number) {
        return a + (b - a) * t;
    }

    private logLerp(a: number, b: number, t: number) {
        var clampedT = Math.min(Math.max(t, 0), 1);
        var PI = 3.14159265359;
        var logT = Math.sin(clampedT * PI * 0.5);
        return a + (b - a) * logT;
    }

    private expLerp(a: number, b: number, t: number) {
        var clampedT = Math.min(Math.max(t, 0), 1);
        var PI = 3.14159265359;
        var expT = 1 - Math.cos(clampedT * PI * 0.5);
        return a + (b - a) * expT;
    }

    public setThrottle(value: number) {
        this.throttleAmount = value;
    }

    public setBrake(value: number) {
        this.brakeAmount = value;
    }

    public setBoost(value: boolean) {
        this.isBoostPressed = value;
    }

    public gearUp() {
        this.currentGear = Math.min(this.currentGear + 1, 7);
        this.clutchSlip = 1.0;
        this.oldRPM = this.rpm;
    }

    public gearDown() {
        this.currentGear = Math.max(this.currentGear - 1, 0);
        this.clutchSlip = 1.0;
        this.oldRPM = this.rpm;
    }

    public setSteer(value: number) {
        var steerValue = Math.abs(value) < 0.03 ? 0 : value;
        this.steerAmount = Math.min(Math.max(steerValue, -1), 1);
    }

    /**
     * Returns a engine torque for a given RPM 
     * @param rpm: The engine RPM
     */
    private getTorqueForRPM(rpm: number) {
        return this.logLerp(this.engineMinTorque, this.engineMaxTorque, rpm / this.maxRPM);
    }


    /**
     * Updates the car physics
     * @param deltaTime: the time delta in seconds with the previous update
     */
    public update(deltaTime: number) {

        this.updateGearbox();

        this.updateEngine(deltaTime);

        this.updateSteering(deltaTime);

        this.updateEngineSound()

        this.updateSurfaceRollingResistance();

        for (var i = 0; i < this.exhaustParticles.length; i++) {
            this.exhaustParticles[i].emitter = this.mesh.position.add(this.mesh.calcMovePOV(i == 0 ? 0.6 : -0.6, 0.262, -2.465));
            this.exhaustParticles[i].direction1 = this.mesh.calcMovePOV(0, 0, -1);
            this.exhaustParticles[i].direction2 = this.mesh.calcMovePOV(0, 0, -1);

            if (this.isBoostPressed && this.boostQuantity > 0) {
                this.exhaustParticles[i].color1 = new BABYLON.Color4(1, 0, 0, 1.0);
                this.exhaustParticles[i].color2 = new BABYLON.Color4(1, 1, 0, 1.0);
                this.exhaustParticles[i].colorDead = new BABYLON.Color4(0, 0, 0, 0.0);
                this.exhaustParticles[i].emitRate = this.exhaustParticles[i].getCapacity();
                this.exhaustParticles[i].minLifeTime = 0.3;
                this.exhaustParticles[i].maxLifeTime = 0.6;
            }
            else {
                this.exhaustParticles[i].color1 = new BABYLON.Color4(1, 1, 1, 1.0);
                this.exhaustParticles[i].color2 = new BABYLON.Color4(1, 1, 1, 1.0);
                this.exhaustParticles[i].colorDead = new BABYLON.Color4(0, 0, 0, 0.0);
                this.exhaustParticles[i].emitRate = this.expLerp(100, 0, this.speedKPH / 30);
                this.exhaustParticles[i].minLifeTime = 0.2;
                this.exhaustParticles[i].maxLifeTime = 0.4;
            }
        }
    }


    /**
     * Updates the automatic gear
     */
    private updateGearbox(): void {
        if (this.rpm > this.maxRPM * 0.95 && this.currentGear > 1 && this.clutchSlip < 0.05)
            this.gearUp();
        else if (this.rpm < this.maxRPM * 0.65 && this.currentGear > 2 && this.clutchSlip < 0.05)
            this.gearDown();
    }


    /**
     * Updates the engine forces and brakes
     * @param deltaTime: the time delta in seconds with the previous update
     */
    private updateEngine(deltaTime: number): void {
        this.forward = this.mesh.calcMovePOV(0, 0, -1);
        
        this.speed = this.forward.scale(BABYLON.Vector3.Dot(this.mesh.physicsImpostor.getLinearVelocity(), this.forward)).length() * 2;
        this.speedKPH = this.speed * 3.6;

        var fTraction: BABYLON.Vector3 = BABYLON.Vector3.Zero();
        var fDrag: BABYLON.Vector3 = this.forward.scale(this.cDrag * (this.speed * this.speed));
        var fRr: BABYLON.Vector3 = this.forward.scale(this.currentRr * this.speed);

        this.rpm = Math.max(Math.abs((this.speed / this.wheelRadius) * this.gearRatios[this.currentGear] * this.differentialRatio * (60 / (2 * Math.PI))), 1000);

        if (this.rearLightsMaterial != null) {
            this.rearLightsMaterial.emissiveTexture = null;
        }

        if (Math.abs(this.speed) > 0.1) {
            if (this.currentGear > 0) {
                if (this.throttleAmount > 0.03) {
                    var length = (this.getTorqueForRPM(this.rpm) * (1 - this.clutchSlip) * this.gearRatios[this.currentGear] * this.differentialRatio * this.transmissionEfficiency * this.throttleAmount) / this.wheelRadius;

                    if (this.isBoostPressed && this.boostQuantity > 0)
                        length *= this.boostTorqueMultiplier;

                    fTraction = this.forward.scale(-length);
                }
                else if (this.brakeAmount > 0.03) {
                    var length = this.brakeForce * this.brakeAmount;
                    fTraction = this.forward.scale(length);

                    if (this.rearLightsMaterial != null) this.rearLightsMaterial.emissiveTexture = this.rearLightsMaterial.albedoTexture;
                }
                else {
                    var length = this.engineVaccum * (this.rpm / this.maxRPM);
                    fTraction = this.forward.scale(length);
                }
            }
            else {
                if (this.brakeAmount > 0.03) {
                    var length = (this.getTorqueForRPM(this.rpm) * (1 - this.clutchSlip) * this.gearRatios[this.currentGear] * this.differentialRatio * this.transmissionEfficiency * this.brakeAmount) / this.wheelRadius;

                    if (this.isBoostPressed && this.boostQuantity > 0)
                        length *= this.boostTorqueMultiplier;

                    fTraction = this.forward.scale(-length);
                }
                else if (this.throttleAmount > 0.03) {
                    var length = this.brakeForce * this.throttleAmount;
                    fTraction = this.forward.scale(length);

                    if (this.rearLightsMaterial != null) this.rearLightsMaterial.emissiveTexture = this.rearLightsMaterial.albedoTexture;
                }
                else {
                    var length = this.engineVaccum * (this.rpm / this.maxRPM);
                    fTraction = this.forward.scale(length);
                }
            }
        }
        else {
            if (this.throttleAmount > 0.03) {
                this.currentGear = 2;
                fTraction = this.forward.scale(-1000);
                this.speed = 1;
            }
            else if (this.brakeAmount > 0.03) {
                this.currentGear = 0;
                fTraction = this.forward.scale(1000);
                this.speed = 1;
            }
            else {
                this.currentGear = 1;
                this.speed = 0;
            }
        }

        if (this.isBoostPressed) {
            this.boostQuantity = Math.max(0, this.boostQuantity - (deltaTime * 0.25));
        }
        else {
            this.boostQuantity = Math.min(1, this.boostQuantity + (deltaTime * 0.066667));
        }


        var fLong: BABYLON.Vector3 = BABYLON.Vector3.Zero(); // Total longitudinal force (traction force + drag force + rolling resistance force)
        
        var maxGearSpeed = (this.maxRPM / this.gearRatios[this.currentGear] / this.differentialRatio) * this.wheelRadius * 0.10472;
        this.speed = Math.min(this.speed, maxGearSpeed);
        
        if (this.currentGear > 1) {
            fLong = fTraction.add(fDrag).add(fRr);
            this.velocity = this.forward.scale(-this.speed);
        }
        else if (this.currentGear == 0) {
            fLong = fTraction.add(fDrag).add(fRr).scale(-1);
            this.velocity = this.forward.scale(this.speed);
        }
        
        this.speedKPH = this.speed * 3.6;
        
        this.velocity = this.velocity.add(new BABYLON.Vector3(0, -9.81 * deltaTime, 0));
        this.velocity = this.velocity.add(fLong.scale(1 / this.mass).scale(deltaTime));

        if (this.currentGear != 1) {
            this.mesh.physicsImpostor.setLinearVelocity(this.velocity.scale(0.5));
        }
        else {
            this.mesh.physicsImpostor.setLinearVelocity(BABYLON.Vector3.Zero());
        }
        this.mesh.physicsImpostor.setAngularVelocity(BABYLON.Vector3.Zero());
    }


    /**
     * Updatse car steering, rotation, and wheel rotations
     * @param deltaTime: the time delta in seconds with the previous update
     */
    private updateSteering(deltaTime: number): void {

        var rot = this.mesh.rotationQuaternion.toEulerAngles().y;

        var maxSteerPercentage: number = this.logLerp(this.maxSteeringAngle, this.maxSteeringAngle * 0.1, this.speedKPH / this.maxSpeedKPH);

        var aimedAngle: number = this.lerpUnclamped(0, this.maxSteeringAngle * maxSteerPercentage, this.steerAmount);
        this.steeringAngle = this.lerp(this.steeringAngle, aimedAngle, 1 - Math.pow(0.05, deltaTime));

        if (Math.abs(this.steeringAngle) < 0.0004)
            this.steeringAngle = 0;

        var radius = this.wheelDistance / Math.sin(this.steeringAngle);
        var angularVelocity: number = 0;
        if (this.currentGear > 0) angularVelocity = this.speed / radius;
        else angularVelocity = (-this.speed) / radius;

        this.mesh.rotationQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(rot + angularVelocity * deltaTime, 0, 0);

        this.clutchSlip = this.lerp(this.clutchSlip, 0, 1 - Math.pow(0.01, deltaTime));
        this.lerpedRPM = this.lerp(this.oldRPM, this.rpm, (1 - this.clutchSlip));

        var wheelMeshRadius = 1.0;

        for (var i = 0; i < this.wheels.length; i++) {
            this.wheels[i].rotation.x += (this.currentGear == 0 ? 1 : -1) * this.speed / (2 * Math.PI * wheelMeshRadius);
        }

        for (var i = 0; i < this.frontWheels.length; i++) {
            this.frontWheels[i].rotation.y = this.steeringAngle;
        }
    }


    /**
     * Speeds up engine sound relative to the current engine RPM 
     */
    private updateEngineSound(): void {
        this.enginesound.setPlaybackRate(0.5 + 0.5 * (this.lerpedRPM / this.maxRPM));
    }


    /**
     * Detects the surface the each wheel is rolling on, and modifiy the rolling resistance accordingly
     */
    private updateSurfaceRollingResistance(): void {

        var color = surface === 1 ? new BABYLON.Color4(0, 1, 0, 1.0) : new BABYLON.Color4(0.87, 0.87, 0.61, 1.0);

        for (var i = 0; i < this.wheels.length; i++) {

            var start: BABYLON.Vector3 = this.mesh.position.add(this.mesh.calcMovePOV(this.wheelsPositions[i].x, this.wheelsPositions[i].y + 0.5, this.wheelsPositions[i].z));
            var end: BABYLON.Vector3 = this.mesh.position.add(this.mesh.calcMovePOV(this.wheelsPositions[i].x, this.wheelsPositions[i].y - 0.5, this.wheelsPositions[i].z));

            var rayPick = BABYLON.Ray.CreateNewFromTo(start, end);
            var meshFound: BABYLON.PickingInfo = this.mesh.getScene().pickWithRay(rayPick, (mesh: BABYLON.Mesh) => {
                if (mesh.name != "cube0" && mesh.name.substring(0, 6) != "mondeo" && mesh.name != "skyBox") return true;
                return false;
            }, true);


            var wheelRr: number = this.cRr;
            var color: BABYLON.Color4;
            var surface: number = 0;

            if (meshFound.hit) {
                if (meshFound.pickedMesh.name === "grass") {
                    wheelRr = this.cRr * 50;
                    color = new BABYLON.Color4(0.30, 0.54, 0.19, 1.0);
                    surface = 1;
                }
                else if (meshFound.pickedMesh.name === "sand") {
                    wheelRr = this.cRr * 100;
                    color = new BABYLON.Color4(0.87, 0.87, 0.61, 1.0);
                    surface = 2;
                }
            }

            this.currentRr += wheelRr;

            this.wheelsParticles[i].emitter = this.mesh.position.add(this.mesh.calcMovePOV(this.wheelsPositions[i].x, this.wheelsPositions[i].y, this.wheelsPositions[i].z));
            this.wheelsParticles[i].direction1 = this.mesh.calcMovePOV(0, 1, -1).normalize();
            this.wheelsParticles[i].direction2 = this.mesh.calcMovePOV(0, 1, -1).normalize();
            this.wheelsParticles[i].color1 = color;
            this.wheelsParticles[i].color2 = color;
            this.wheelsParticles[i].colorDead = new BABYLON.Color4(0, 0, 0, 0.0);
            this.wheelsParticles[i].emitRate = surface === 0 ? 0 : (this.speedKPH / this.maxSpeedKPH) * this.wheelsParticles[i].getCapacity();
        }

        this.currentRr /= this.wheels.length;
    }


    public enableParticles(): void {

        for (var i = 0; i < this.wheelsParticles.length; i++) {
            if (!this.wheelsParticles[i].isStarted())
                this.wheelsParticles[i].start();
        }

        for (var i = 0; i < this.exhaustParticles.length; i++) {
            if (!this.exhaustParticles[i].isStarted()) {
                this.exhaustParticles[i].start();
            }
        }
    }


    public disableParticles(): void {

        for (var i = 0; i < this.wheelsParticles.length; i++) {
            if (this.wheelsParticles[i].isStarted())
                this.wheelsParticles[i].stop();
        }

        for (var i = 0; i < this.exhaustParticles.length; i++) {
            if (this.exhaustParticles[i].isStarted())
                this.exhaustParticles[i].stop();
        }
    }


    public enableEngineSound(): void {

        if (!this.enginesound.isPlaying)
            this.enginesound.play();
    }


    public disableEngineSound(): void {

        if (this.enginesound.isPlaying)
            this.enginesound.stop();
    }
}
