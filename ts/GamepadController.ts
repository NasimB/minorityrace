class GamepadController {

    private gamepad: BABYLON.Gamepad;
    private xbox360pad: BABYLON.Xbox360Pad;
    private gamepads: BABYLON.Gamepads;

    public rightTrigger: number = 0;
    public leftTrigger: number = 0;
    public directionX: number = 0;
    public boost: boolean = false;

    public isGamepadConnected: boolean = false;
    
    constructor() {
        this.gamepads = new BABYLON.Gamepads(this.gamepadConnected.bind(this));
    }

    private gamepadConnected(gamepad: BABYLON.Gamepad) {
        console.log("[INFO] Nouvelle manette detectee.");
        if (gamepad instanceof BABYLON.Xbox360Pad) {
            this.isGamepadConnected = true;
            this.xbox360pad = gamepad;

            this.xbox360pad.onbuttondown((button) => {
                switch (button) {
                    case 0:
                        this.boost = true;
                        break;
                    case 2:
                        this.boost = true;
                        break;
                }
            });

            this.xbox360pad.onbuttonup((button) => {
                switch (button) {
                    case 0:
                        this.boost = false;
                        break;
                    case 2:
                        this.boost = false;
                        break;
                }
            });

            console.log("[INFO] Manette configuree avec succes."); 
        }
        else {
            console.log("[INFO] Manette non utilisable, seules les manettes Xbox 360 (ou detectees comme tels) sont compatibles.");
        }
    }


    public update() {
        if (this.isGamepadConnected) {

            this.directionX = this.xbox360pad.leftStick.x;

            this.rightTrigger = this.xbox360pad.rightTrigger;
            this.leftTrigger = this.xbox360pad.leftTrigger;
        }
    }
}
