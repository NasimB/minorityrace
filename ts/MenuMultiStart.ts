class MenuMultiStart {

    public isReady: boolean = false;

    private titleText: BABYLON.Text2D;

    private multiList: BABYLON.Rectangle2D;
    private readyButton: BABYLON.Rectangle2D;
    private backButton: BABYLON.Rectangle2D;
    private playerList: BABYLON.Text2D;
    private serverStateText: BABYLON.Text2D;

    public isVisible: boolean = false;

    public onReadyButtonClicked: () => void;
    public onBackButtonClicked: () => void;

    constructor(scene: BABYLON.Scene, canvas: BABYLON.ScreenSpaceCanvas2D) {

        this.titleText = new BABYLON.Text2D("Multiplayer", { parent: canvas, marginAlignment: "h: center, v: top", fontName: "40pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y: -150 });
    
        this.readyButton = new BABYLON.Rectangle2D({
            id: "readyButton", x: 0, y: -100, width: 300, height: 40,
            fill: "#000000FF", roundRadius: 10,
            children:
            [
                new BABYLON.Text2D("Ready", { marginAlignment: "h: center, v:center" })
            ]

        });

        this.readyButton.pointerEventObservable.add((d, s) => {
            this.onReadyButtonClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);


        this.backButton = new BABYLON.Rectangle2D({
            id: "backButton", x: 0, y: -150, width: 300, height: 40,
            fill: "#000000FF", roundRadius: 10,
            children:
            [
                new BABYLON.Text2D("Back", { marginAlignment: "h: center, v:center" })
            ]

        });

        this.backButton.pointerEventObservable.add((d, s) => {
            this.onBackButtonClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);


        this.serverStateText = new BABYLON.Text2D("Waiting for players to get ready...", { marginAlignment: "h: center, v: center", fontName: "14pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y: -160 });
        
        this.playerList = new BABYLON.Text2D("Players:", { marginAlignment: "h: left, v:top", x:10, y:-10 });

        this.multiList = new BABYLON.Rectangle2D({
            parent: canvas, id: "mulitList", width: 300, height: 250,
            fill: "#000000FF", roundRadius: 10,
            marginAlignment: "h: center, v: center",
            children:
            [
                this.playerList,
                this.readyButton,
                this.serverStateText,
                this.backButton,
            ]
        }); 
    }


    public setPlayerList(list: any[]): void {
        var str: string = "Players:\n\n";

        if (list != null) {
            for (var i = 0; i < list.length; i++) {
                var readyStr = list[i].isReady ? "[READY]" : "";
                var inRaceStr = list[i].isInRace ? "[IN RACE]" : "";
                var youStr = list[i].id == Socket.id ? " (You)" : "";
                str += readyStr + inRaceStr + " - " + list[i].name + " (" + list[i].id + ")" + youStr + "\n\n";
            }
        }
        this.playerList.text = str;
    }


    public setServerState(state: number): void {
        switch (state) {
            case 0: this.serverStateText.text = "Waiting for players to get ready..."; break;
            case 1: this.serverStateText.text = "A race is starting..."; break;
            case 2: this.serverStateText.text = "A race is in progress..."; break;
            case 3: this.serverStateText.text = "A race is ending..."; break;
        }
    }


    public hide(): void {
        this.titleText.levelVisible = false;
        this.multiList.levelVisible = false;
        this.readyButton.levelVisible = false;
        this.backButton.levelVisible = false;
        this.isVisible = false;
    }


    public show(): void {
        this.titleText.levelVisible = true;
        this.multiList.levelVisible = true;
        this.readyButton.levelVisible = true;
        this.backButton.levelVisible = true;
        this.isVisible = true;
    }
}