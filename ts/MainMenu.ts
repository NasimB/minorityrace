class MainMenu {

    private soloButton: BABYLON.Rectangle2D;
    private multiButton: BABYLON.Rectangle2D;
    private controlsButton: BABYLON.Rectangle2D;

    private title: BABYLON.Text2D;
    
    public onSingleplayerClicked: () => void;
    public onMultiplayerClicked: () => void;
    public onControlsClicked: () => void;

    public isVisible: boolean = false;

    constructor(scene: BABYLON.Scene, canvas: BABYLON.ScreenSpaceCanvas2D) {

        this.soloButton = new BABYLON.Rectangle2D(
            {
                parent: canvas, id: "button", x: 0, y: 75, width: 300, height: 40,
                fill: "#000000FF", roundRadius: 10,
                marginAlignment: "h: center, v: center",
                children:
                [
                    new BABYLON.Text2D("Singleplayer", { marginAlignment: "h: center, v: center" }),
                ]
            });

        this.soloButton.pointerEventObservable.add((d, s) => {
            this.onSingleplayerClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);


        this.multiButton = new BABYLON.Rectangle2D(
            {
                parent: canvas, id: "button2", x: 0, y: 0, width: 300, height: 40,
                fill: "#000000FF", roundRadius: 10,
                marginAlignment: "h: center, v: center",
                children:
                [
                    new BABYLON.Text2D("Multiplayer", { marginAlignment: "h: center, v: center" })
                ]
            });

        this.multiButton.pointerEventObservable.add((d, s) => {
            this.onMultiplayerClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);


        this.controlsButton = new BABYLON.Rectangle2D(
            {
                parent: canvas, id: "button2", x: 0, y: -75, width: 300, height: 40,
                fill: "#000000FF", roundRadius: 10,
                marginAlignment: "h: center, v: center",
                children:
                [
                    new BABYLON.Text2D("Controls", { marginAlignment: "h: center, v: center" })
                ]
            });

        this.controlsButton.pointerEventObservable.add((d, s) => {
            this.onControlsClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);
    };


    public dispose(): void {
        this.soloButton.dispose();
        this.multiButton.dispose();
    }


    public hide(): void {
        this.soloButton.levelVisible = false;
        this.multiButton.levelVisible = false;
        this.controlsButton.levelVisible = false;
        this.isVisible = false;
    }


    public show(): void {
        this.soloButton.levelVisible = true;
        this.multiButton.levelVisible = true;
        this.controlsButton.levelVisible = true;
        this.isVisible = true;
    }
}
