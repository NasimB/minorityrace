class KeyboardController {

    public isThrottlePressed: boolean = false;
    public isBrakePressed: boolean = false;
    public isSteerLeftPressed: boolean = false;
    public isSteerRightPressed: boolean = false;
    public isResetPressed: boolean = false;
    public isBoostPressed: boolean = false;

    constructor() {
        window.addEventListener("keydown", (event) => {
            this.onKeyDown(event);
        });

        window.addEventListener("keyup", (event) => {
            this.onKeyUp(event);
        });
    }

    private onKeyDown(evt) {
        switch (evt.keyCode) {
            case 90: //'Z'
                this.isThrottlePressed = true;
                break;
            case 38: // Up arrow
                this.isThrottlePressed = true;
                break;
            case 83: //'S'
                this.isBrakePressed = true;
                break;
            case 40: // Down arrow
                this.isBrakePressed = true;
                break;
            case 81: //'Q'
                this.isSteerLeftPressed = true;
                break;
            case 37: // Left arrow
                this.isSteerLeftPressed = true;
                break;
            case 68: //'Q'
                this.isSteerRightPressed = true;
                break;
            case 39: // Right arrow
                this.isSteerRightPressed = true;
                break;
            case 46: //'Suppr'
                this.isResetPressed = true;
                break;
            case 32: //'Space Bar'
                this.isBoostPressed = true;
                break;
        }
    }

    private onKeyUp(evt) {
        switch (evt.keyCode) {
            case 90: //'Z'
                this.isThrottlePressed = false;
                break;
            case 38: // Up arrow
                this.isThrottlePressed = false;
                break;
            case 83: //'S'
                this.isBrakePressed = false;
                break;
            case 40: // Down arrow
                this.isBrakePressed = false;
                break;
            case 81: //'Q'
                this.isSteerLeftPressed = false;
                break;
            case 37: // Left arrow
                this.isSteerLeftPressed = false;
                break;
            case 68: //'Q'
                this.isSteerRightPressed = false;
                break;
            case 39: // Right arrow
                this.isSteerRightPressed = false;
                break;
            case 46: //'Suppr'
                this.isResetPressed = false;
                break;
            case 32: //'Space Bar'
                this.isBoostPressed = false;
                break;
        } 
    }
}
