declare var socket;
declare var io;

class Socket {

    public static id: number = -1;
    public static isConnected: boolean = false;
    
    public static connect() {
        socket = io.connect("http://87.88.79.196:8888");

        socket.on("ok", (info) => {
            this.id = info.id;
            this.isConnected = true;
            console.log("[INFO] Connected to the server");
        });
    }

    public static disconnect() {
        socket.disconnect();
        this.isConnected = false;
        this.id = -1;
        console.log("[INFO] Disconnected from the server");
    }

    public static emit(eventName: string, params) {
        socket.emit(eventName, params);
    }

    public static on(eventName: string, callback) {
        socket.on(eventName, callback);
    }
}
