class MenuControls {

    public isVisible: boolean = false;

    public onBackButtonClicked: () => void;

    private titleText: BABYLON.Text2D;
    private backButton: BABYLON.Rectangle2D;
    private controlsSprite: BABYLON.Sprite2D;
    private controlsTexture: BABYLON.Texture;

    private keyboardText: BABYLON.Text2D;
    private gamepadText: BABYLON.Text2D;
    private throttleText: BABYLON.Text2D;
    private brakeText: BABYLON.Text2D;
    private turnLeftText: BABYLON.Text2D;
    private turnRightText: BABYLON.Text2D;
    private boostText: BABYLON.Text2D;
    

    constructor(scene: BABYLON.Scene, canvas: BABYLON.ScreenSpaceCanvas2D) {
        this.titleText = new BABYLON.Text2D("Controls", { parent: canvas, marginAlignment: "h: center, v: top", fontName: "40pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y:-150 });
        
        this.controlsTexture = new BABYLON.Texture("assets/menus/controls.png", scene);
        this.controlsTexture.hasAlpha = true;

        var scaleWidth = (window.innerWidth / 1920) * 0.8;
        var scaleHeight = (window.innerHeight / 1080) * 0.8;
        var scale = Math.min(scaleWidth, scaleHeight);
        
        this.controlsSprite = new BABYLON.Sprite2D(this.controlsTexture, {
            parent: canvas, x: -100 * scale, y: 0, marginAlignment: "h: center, v: center", scale: scale, alignToPixel:true
        });

        this.keyboardText = new BABYLON.Text2D("Keyboard", { parent: this.controlsSprite, marginAlignment: "h: left, v: center", fontName: "20pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y: (416 / 2) + 25, x: 25 });
        this.gamepadText = new BABYLON.Text2D("Gamepad", { parent: this.controlsSprite, marginAlignment: "h: left, v: center", fontName: "20pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y: (416 / 2) + 25, x: 175 });

        this.throttleText = new BABYLON.Text2D("Throttle", { parent: this.controlsSprite, marginAlignment: "h: left, v: center", fontName: "20pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y: (416 / 2) - 40, x: 320 });
        this.brakeText = new BABYLON.Text2D("Brake", { parent: this.controlsSprite, marginAlignment: "h: left, v: center", fontName: "20pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y: (416 / 2) - 130, x: 320 });
        this.turnLeftText = new BABYLON.Text2D("Turn Left", { parent: this.controlsSprite, marginAlignment: "h: left, v: center", fontName: "20pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y: (416 / 2) - 220, x: 320 });
        this.turnRightText = new BABYLON.Text2D("Turn Right", { parent: this.controlsSprite, marginAlignment: "h: left, v: center", fontName: "20pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y: (416 / 2) - 305, x: 320 });
        this.boostText = new BABYLON.Text2D("Boost", { parent: this.controlsSprite, marginAlignment: "h: left, v: center", fontName: "20pt Arial", defaultFontColor: new BABYLON.Color4(0, 0, 0, 1), y: (416 / 2) - 390, x: 320 });


        this.backButton = new BABYLON.Rectangle2D({
            parent: canvas, id: "button2", width: 300, height: 40,
            fill: "#000000FF", roundRadius: 10, y: 150,
            marginAlignment: "h: center, v: bottom",
            children:
            [
                new BABYLON.Text2D("Back", { marginAlignment: "h: center, v: center" })
            ]
        });

        this.backButton.pointerEventObservable.add((d, s) => {
            this.onBackButtonClicked();
        }, BABYLON.PrimitivePointerInfo.PointerUp);
    }


    public hide(): void {
        this.isVisible = false;
        this.titleText.levelVisible = false;
        this.backButton.levelVisible = false;
        this.controlsSprite.levelVisible = false;
        this.keyboardText.levelVisible = false;
        this.gamepadText.levelVisible = false;
        this.throttleText.levelVisible = false;
        this.brakeText.levelVisible = false;
        this.turnLeftText.levelVisible = false;
        this.turnRightText.levelVisible = false;
        this.boostText.levelVisible = false;
    }


    public show(): void {
        this.isVisible = true;
        this.titleText.levelVisible = true;
        this.backButton.levelVisible = true;
        this.controlsSprite.levelVisible = true;
        this.keyboardText.levelVisible = true;
        this.gamepadText.levelVisible = true;
        this.throttleText.levelVisible = true;
        this.brakeText.levelVisible = true;
        this.turnLeftText.levelVisible = true;
        this.turnRightText.levelVisible = true;
        this.boostText.levelVisible = true;
    }
}