/// <reference path=".\typings\babylon.d.ts" />
/// <reference path=".\Socket.ts" />
/// <reference path=".\MainMenu.ts" />
/// <reference path=".\MenuMultiStart.ts" />
/// <reference path=".\MenuSoloEnd.ts" />
/// <reference path=".\MenuMultiEnd.ts" />
/// <reference path=".\MenuControls.ts" />

class MenuManager {

    private mainMenu: MainMenu;
    private menuMultiStart: MenuMultiStart;
    private menuSoloEnd: MenuSoloEnd;
    private menuMultiEnd: MenuMultiEnd;
    private menuControls: MenuControls;

    public onLaunchSingleplayerRace: () => void;
    public onLaunchMultiplayerRace: (date: number) => void;

    public canvas: BABYLON.ScreenSpaceCanvas2D;

    public menuBackground: BABYLON.Rectangle2D;
    public titleText: BABYLON.Text2D;
    public creditText: BABYLON.Text2D;

    public hud: Hud;

    private playerList: any[] = null;

    constructor() {

    }


    public initMenu(scene: BABYLON.Scene) {
        // Init HUD
        this.hud = new Hud(scene);
        this.hud.initHud(scene);

        // Init Menus
        this.canvas = this.createCanvas(scene);

        window.addEventListener('resize', () => {
            if (this.hud != null) {
                this.hud.dispose();
            }

            this.hud = new Hud(scene);
            this.hud.initHud(scene);

            if (this.canvas != null) {
                this.canvas.dispose();
            }
            this.canvas = this.createCanvas(scene);
            this.menuMultiStart.setPlayerList(this.playerList);
        });

        this.mainMenu.show();
        this.menuMultiStart.hide();
        this.menuMultiEnd.hide();
        this.menuSoloEnd.hide();
        this.menuControls.hide();
    }


    public createCanvas(scene: BABYLON.Scene): BABYLON.Canvas2D {

        var mainMenuActive: boolean = false;
        var menuMultiStartActive: boolean = false;
        var menuSoloEndActive: boolean = false;
        var menuMultiEndActive: boolean = false;
        var menuControlsActive: boolean = false;
        var canvasActive: boolean = true;

        if (this.canvas != null) {
            canvasActive = this.canvas.levelVisible;
        }
        if (this.mainMenu != null) {
            mainMenuActive = this.mainMenu.isVisible;
        }
        if (this.menuMultiStart != null) {
            menuMultiStartActive = this.menuMultiStart.isVisible;
        }

        if (this.menuMultiEnd != null) {
            menuMultiEndActive = this.menuMultiEnd.isVisible;
        }

        if (this.menuSoloEnd != null) {
            menuSoloEndActive = this.menuSoloEnd.isVisible;
        }

        if (this.menuControls != null) {
            menuControlsActive = this.menuControls.isVisible;
        }

        var tmpCanvas: BABYLON.Canvas2D = new BABYLON.ScreenSpaceCanvas2D(scene, { id: "MenuCanvas", size: new BABYLON.Size(window.innerWidth, window.innerHeight) });

        this.menuBackground = new BABYLON.Rectangle2D({
            parent: tmpCanvas,
            x: 0,
            y: 0,
            width:
            window.innerWidth,
            height: window.innerHeight,
            fill: "#FFFFFFFF",
            marginAlignment: "h: center, v: center"
        });

        var scaleWidth = (window.innerWidth / 1920);
        var scaleHeight = (window.innerHeight / 1080);
        var scale = Math.min(scaleWidth, scaleHeight);

        this.titleText = new BABYLON.Text2D("MINORITY RACE", {
            parent: this.menuBackground,
            marginAlignment: "h: center, v:top",
            fontName: Math.floor(scale * 60) + "pt Arial Black",
            defaultFontColor: new BABYLON.Color4(0, 0, 0, 1),
            y: -50 * scale
        });

        this.creditText = new BABYLON.Text2D("Made by:\nNasim BOUGUERRA\nPaul LOUMOUAMOU\nMusic by:\nFabio CENTRACCHIO", {
            parent: this.menuBackground,
            marginAlignment: "h: left, v:bottom",
            fontName: Math.floor(scale * 18) + "pt Arial",
            defaultFontColor: new BABYLON.Color4(0, 0, 0, 1),
            x: 5 * scale,
            y: 5 * scale
        });

        this.initMainMenu(scene, tmpCanvas);

        this.initMenuMultiStart(scene, tmpCanvas);

        this.initMenuMultiEnd(scene, tmpCanvas);

        this.initMenuSoloEnd(scene, tmpCanvas);

        this.initMenuControls(scene, tmpCanvas);

        mainMenuActive ? this.mainMenu.show() : this.mainMenu.hide();
        menuMultiStartActive ? this.menuMultiStart.show() : this.menuMultiStart.hide();
        menuMultiEndActive ? this.menuMultiEnd.show() : this.menuMultiEnd.hide();
        menuSoloEndActive ? this.menuSoloEnd.show() : this.menuSoloEnd.hide();
        menuControlsActive ? this.menuControls.show() : this.menuControls.hide();
        tmpCanvas.levelVisible = canvasActive;
        return tmpCanvas;
    }


    public initMainMenu(scene: BABYLON.Scene, canvas: BABYLON.ScreenSpaceCanvas2D) {
        this.mainMenu = new MainMenu(scene, canvas);

        this.mainMenu.onSingleplayerClicked = () => {
            this.mainMenu.hide();
            this.canvas.levelVisible = false;
            this.onLaunchSingleplayerRace();
            this.menuSoloEnd.show();
        };

        this.mainMenu.onMultiplayerClicked = () => {
            this.mainMenu.hide();

            Socket.connect();

            Socket.on("ok", (info) => {
                this.menuMultiStart.show();
                this.menuMultiStart.setServerState(info.state);
            });

            Socket.on("playerconnected", (playerid, playerlist) => {
                this.menuMultiStart.setPlayerList(playerlist);
                this.playerList = playerlist;
            });

            Socket.on("playerdisconnected", (playerid, playerlist) => {
                this.menuMultiStart.setPlayerList(playerlist);
                this.playerList = playerlist;
            });

            Socket.on("playerlist", (playerlist) => {
                this.menuMultiStart.setPlayerList(playerlist);
                this.playerList = playerlist;
            });

            Socket.on("playerready", (playerid, playerlist) => {
                this.menuMultiStart.setPlayerList(playerlist);
                this.playerList = playerlist;
            });

            Socket.on("launchrace", (date) => {
                this.menuMultiStart.hide();
                this.canvas.levelVisible = false;
                this.onLaunchMultiplayerRace(date);
            });
        };

        this.mainMenu.onControlsClicked = () => {
            this.mainMenu.hide();
            this.menuControls.show();
        };
    }


    public initMenuMultiStart(scene: BABYLON.Scene, canvas: BABYLON.ScreenSpaceCanvas2D) {
        this.menuMultiStart = new MenuMultiStart(scene, canvas);
        this.menuMultiStart.onReadyButtonClicked = this.onReadyButtonClicked;
        this.menuMultiStart.onBackButtonClicked = () => {
            this.menuMultiStart.hide();
            this.mainMenu.show();
            Socket.disconnect();
        };
    }


    private onReadyButtonClicked(): void {
        Socket.emit("ready", null);
    }


    public initMenuSoloEnd(scene: BABYLON.Scene, canvas: BABYLON.ScreenSpaceCanvas2D) {
        this.menuSoloEnd = new MenuSoloEnd(scene, canvas);
        this.menuSoloEnd.onMainMenuButtonClicked = () => {
            this.menuSoloEnd.hide();
            this.mainMenu.show();
        };
    }


    public initMenuMultiEnd(scene: BABYLON.Scene, canvas: BABYLON.ScreenSpaceCanvas2D) {
        this.menuMultiEnd = new MenuMultiEnd(scene, canvas);
        this.menuMultiEnd.onMainMenuButtonClicked = () => {
            this.menuMultiEnd.hide();
            this.mainMenu.show();
            Socket.disconnect();
        };
    }


    public initMenuControls(scene: BABYLON.Scene, canvas: BABYLON.ScreenSpaceCanvas2D) {
        this.menuControls = new MenuControls(scene, canvas);

        this.menuControls.onBackButtonClicked = () => {
            this.menuControls.hide();
            this.mainMenu.show();
        };
    }


    public onEndCountdownFinished(): void {
        this.canvas.levelVisible = true;
        this.menuMultiEnd.show();

        Socket.emit("getendlist", null);
        Socket.on("endlist", (list) => {
            this.menuMultiEnd.setRank(list);
        });

        this.menuMultiEnd.onRetryButtonClicked = () => {
            this.menuMultiEnd.hide();
            this.menuMultiStart.show();
        };
    }


    public onSoloRaceFinished(times: string[]): void {
        this.canvas.levelVisible = true;
        this.menuSoloEnd.show();
        this.menuSoloEnd.firstTime.text = times[0];
        this.menuSoloEnd.secondTime.text = times[1];
        this.menuSoloEnd.thirdTime.text = times[2];
        this.menuSoloEnd.onRetryButtonClicked = () => {
            this.menuSoloEnd.hide();
            this.canvas.levelVisible = false;
            this.onLaunchSingleplayerRace();
        };
    }
}
